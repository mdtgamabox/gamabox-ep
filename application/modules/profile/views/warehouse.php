<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Profile</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Profile Well</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-warehouse">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Warehouse</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleWarehouse"><strong>Nama Warehouse</strong> | Warehouse ID</h2>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  warehouseId = '<?php echo $warehouseId; ?>';
</script>

 