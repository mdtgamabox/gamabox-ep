<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Profile</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Profile Seismic</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-document">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Seismic</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleSeismic"><strong>Nama Seismic</strong> | Seismic ID</h2>
        <div id="map"></div>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#seismic-tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
          <li><a href="#seismic-tab2" data-toggle="tab"><i class="icon-eye"></i>Coordinate</a></li>
          <li><a href="#seismic-tab3" data-toggle="tab"><i class="icon-doc"></i>Documents</a></li>
          <li><a href="#seismic-tab4" data-toggle="tab"><i class="icon-docs"></i>Seismic Data</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="seismic-tab1">
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="seismic-tab2">
          </div>
          <!-- README TAB 3 -->
          <div class="tab-pane fade" id="seismic-tab3">
          </div>
          <!-- README TAB 4 -->
          <div class="tab-pane fade" id="seismic-tab4">
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>

<script type="text/javascript">
  
        

</script>

<script type="text/javascript">
  seismicId = '<?php echo $seismicId; ?>';
  seismicType = '<?php echo $seismicType; ?>';
</script>

 