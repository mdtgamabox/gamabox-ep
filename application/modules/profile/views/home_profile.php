<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Profile</strong> View</h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('') ?>">Home</a>
      </li>
      <li class="active">Profile</li>
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->


<!-- PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
     <!-- README PANEL -->
         <div class="panel panel-main">
            <div class="panel-header panel-controls">
              <h3><i class="icon-layers"></i><strong>Profile</strong> Page</h3>
            </div>

            <!-- PANEL CONTENT -->
             <div class="panel-content content-profile">
                <?php foreach ($avatar->avatarUser as $data) { ?>
                <h2><strong><?php echo ucfirst($data->userId->userName); ?></strong> | Profile ID</h2>
                <?php } ?>
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
                  <li> <a href="#tab2" data-toggle="tab"><i class="icon-basket"></i>Transaction</a></li>
                  <li> <a href="#tab3" data-toggle="tab"><i class="icon-docs"></i>Document Request</a></li>
                </ul>

                <div class="tab-content tab-head">
                     <!-- PANEL TAB #1 -->               
                    <div class="tab-pane fade active in" id="tab1">
                        <h4> <strong class="text-info">User Profile</strong> </h4>
                        <?php foreach ($avatar->avatarUser as $data){ ?>
                        <div class="row">
                          <div class="col-md-8">
                               <table class="table table-striped">
                                  <tr> 
                                      <td> <strong > Username &nbsp;</strong> </td>
                                      <td> &nbsp; <?php echo $data->userId->userName; ?> </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Email &nbsp; </strong> </td>
                                      <td> &nbsp; <?php echo $data->userId->userEmail; ?> </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Business Associate &nbsp; </strong>  </td>
                                      <td>  &nbsp; <?php echo $data->userId->businessAssociate->baName; ?>  </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Business Associate Type &nbsp; </strong>  </td>
                                      <td>  &nbsp; <?php echo $data->userId->businessAssociate->baType; ?>  </td>
                                  </tr>
                              </table>
                          </div>              
                        </div>
                        <?php } ?>
                    </div>
                    <!-- END PANEL TAB #1 -->

                    <!-- PANEL TAB #2 -->   
                    <div class="tab-pane fade" id="tab2">
                      <!-- README LIST REQUEST USER -->
                      <?php 
                          if ($request_user){
                      ?>
                           <?php
                              foreach ($request_user->requests as $req_user){
                                $request_id = $req_user->requestId; 
                                $request_status = $this->auth_lib->get_request_status($request_id);
                                if ($request_id){
                                   // show request list 
                                   $request_detail =  $this->auth_lib->get_request_detail($request_id);
                          ?>
                              
                          <?php
                                   // declare $request_detail into array
                                    $arr = array();
                                    $arr[] = $request_detail;
                                    foreach ($arr as $det){
                                      // get information request status
                                    $doc_request = $this->auth_lib->get_request_document($request_id);
                          ?>
                                    <h4 class="text-info"> <strong > Request Detail Information - <?php echo $request_id ?> </strong> </h4>
                                   
                                      <ul>
                                          <li > Document Request : </li>
                                              <div class="col-md-offset-1">
                          <?php     foreach ($doc_request->documentsByRequest as $data){ ?>
                                             
                                             <ul>
                                                <?php 
                                                    $doc_req = $this->auth_lib->get_document_user($request_id);
                                                   
                                                    if (empty($doc_req->docUserRoles)){ 
                                                ?>
                                                    <li> <?php  echo $data->documentId->documentName."<br/>";  ?></li>
                                                <?php 
                                                    }
                                                    else {
                                                 ?>
                                                      
                                                      <li> 
                                                          <a href ="<?php echo site_url('profile/document/'.$data->documentId->documentId) ?>" >
                                                               <?php  echo $data->documentId->documentName."<br/>";  ?> 
                                                          </a> 
                                                      </li>
                                             
                                             <?php  } ?>
                                             </ul>
                          <?php
                                      } // end foreach doc_request
                          ?>                  </div>
                                          <li> BA Name Target : <?php echo $data->documentId->businessAssociate->baName; ?> </li>
                                    </ul>
                                  
                                  
                             <table class="table table-hover">  
                                  <thead>
                                      <tr>
                                          <?php   
                                                $type_id = $det->requestType->reqTypeId;
                                                $step_detail = $this->auth_lib->get_request_step($type_id);
                                                foreach ($step_detail->requestTypeSteps as $val){
                                          ?>
                                                <th> <?php  echo $val->stepName; ?> </th>
                                          <?php
                                                  } //end foreach step_detail
                                          ?>
                                      </tr>
                                  </thead>

                                  <tbody>
                                      <tr>
                                          <?php 
                                             foreach ($request_status->requestStatus as $stat){
                                               }
                                          ?>
                                            <td>
                                                <?php
                                                    $request_date = $stat->requestId->requestDate;
                                                    $arr_date = explode("-", $request_date);
                                                    $date= $arr_date[2];
                                                    $month = $arr_date[1];
                                                    $year = $arr_date[0];
                                                    switch ($month){
                                                        case 1 : $month ='Jan'; break;
                                                        case 2 : $month ='Feb'; break;
                                                        case 3 : $month = 'Mar'; break;
                                                        case 4 : $month = 'Apr'; break;
                                                        case 5 : $month = 'May'; break;
                                                        case 6 : $month = 'Jun'; break;
                                                        case 7 : $month = 'Jul'; break;
                                                        case 8 : $month = 'Aug'; break;
                                                        case 9 : $month = 'Sept'; break;
                                                        case 10 : $month = 'Oct'; break;
                                                        case 11 : $month = 'Nov'; break;
                                                        case 12 : $month = 'Dec'; break;
                                                    }
                                                     if ($request_date == null){
                                                        echo "-";
                                                     }
                                                     else {
                                                        echo "Date       : " .$date.' '.$month.' '.$year; echo "<br/>";
                                                     }

                                                     $status = $stat->requestId->status;
                                                     if ($status == "Pending"){
                                                     ?>
                                                        <i class="text-danger"> Status : <?php echo $status; ?> </i> <br/>
                                                     <?php
                                                     }
                                                     else {
                                                     ?>
                                                        <i class="text-info"> Status : <?php echo $status; ?> </i> <br/>
                                                     <?php
                                                     }
                                                     echo "Requestor  : " .$stat->requestId->requestor->userName; echo "<br/>";
                                                ?>
                                            </td>

                                           
                                                <?php
                                                    foreach ($request_status->requestStatus as $stat){
                                                        $approval_date = $stat->approvalId->approvalDate;
                                                      echo "<td>";
                                                        if ($approval_date){
                                                          $arr_date = explode("-", $approval_date);
                                                          $date= $arr_date[2];
                                                          $month = $arr_date[1];
                                                          $year = $arr_date[0];
                                                          switch ($month){
                                                              case 1 : $month ='Jan'; break;
                                                              case 2 : $month ='Feb'; break;
                                                              case 3 : $month = 'Mar'; break;
                                                              case 4 : $month = 'Apr'; break;
                                                              case 5 : $month = 'May'; break;
                                                              case 6 : $month = 'Jun'; break;
                                                              case 7 : $month = 'Jul'; break;
                                                              case 8 : $month = 'Aug'; break;
                                                              case 9 : $month = 'Sept'; break;
                                                              case 10 : $month = 'Oct'; break;
                                                              case 11 : $month = 'Nov'; break;
                                                              case 12 : $month = 'Dec'; break;
                                                          }
                                                          echo "Date       : " .$date.' '.$month.' '.$year; echo "<br/>";
                                                        }
                                                        else{
                                                          echo "Date        : - "; echo "<br/>";
                                                        }
                                                        $status = $stat->approvalId->status;

                                                           if ($status == "Yes"){
                                                       ?>
                                                          <i class="text-info"> Status : <?php echo $status; ?> </i> <br/>
                                                       <?php
                                                       }
                                                       else {
                                                       ?>
                                                          <i class="text-danger"> Status : <?php echo $status; ?> </i> <br/>
                                                       <?php
                                                       }
                                                        echo  "Target : " .$stat->approvalId->target->userName; echo "<br/>"; 
                                                        echo "</td>";
                                                    } 
                                                ?>
                                            
                                      </tr>
                                  </tbody>
                                      
                              </table>
                              <?php 
                                  $cek_dokumen = $this->auth_lib->get_document_user($request_id);
                                  if (empty($cek_dokumen->docUserRoles)){

                                  }
                                  else { 
                              ?>
                                   <a href="<?php echo site_url('profile/transmittal/'.$request_id) ?>" class="btn btn-primary"> 
                                    <i class="icon-docs"></i> Transmittal Letter
                                   </a>
                              <?php
                                  } // end else cek dokumen
                              ?>
                                <?php

                                     } //end foreach detail_request
                                 ?>
                                <hr/>
                        <?php
                              }
                              else {
                                // request list empty
                              }
                            
                            }// end request user foreach
                          ?> 
                      <?php
                          } // end of $request_user
                          else {

                          }
                      ?> 
                      <!-- END REQUEST LIST -->

                      <!-- list approval notification -->
                       <?php 
                          if ($approval_user){
                             foreach ($approval_user->approvals as $app_user){
                                $approval_id = $app_user->approvalId;
                                $app_type_step_req = $app_user->requestTypeStep->reqTypeStepId;
                                $request_id = $app_user->requestId->requestId;
                                $request_status = $this->auth_lib->get_request_status($request_id);
                             
                               if ($request_id){
                                   // show request list 
                                   $request_detail =  $this->auth_lib->get_request_detail($request_id);

                                  // declare $request_detail into array
                                  $arr = array();
                                  $arr[] = $request_detail;
                                  foreach ($arr as $det){
                                    // get information request status
                                    $doc_request = $this->auth_lib->get_request_document($request_id);
                          ?>
                                     <h4 class="text-info"> <strong > Request Detail Information </strong> </h4>
                                   
                                      <ul>
                                          <li > Document Request : </li>
                                              <div class="col-md-offset-1">
                          <?php     foreach ($doc_request->documentsByRequest as $data){ ?>
                                             
                                             <ul>
                                                <li> <?php echo $data->documentId->documentName."<br/>";  ?></li>
                                             </ul>
                          <?php
                                      } // end foreach doc_request
                          ?>                  </div>
                                          <li> BA Name Target : <?php echo $data->documentId->businessAssociate->baName; ?> </li>
                                    </ul>

                                    <table class="table table-hover">
                                         <thead>
                                            <tr>
                                                <?php   
                                                      $type_id = $det->requestType->reqTypeId;
                                                      $step_detail = $this->auth_lib->get_request_step($type_id);

                                                      foreach ($step_detail->requestTypeSteps as $val){
                                                ?>
                                                      <th> <?php  echo $val->stepName; ?> </th>
                                                <?php
                                                        } //end foreach step_detail
                                                ?>
                                            </tr>
                                        </thead>

                                         <tbody>
                                                <tr>
                                                    <?php 
                                                       foreach ($request_status->requestStatus as $stat){
                                                         }
                                                    ?>
                                                      <td>
                                                          <?php
                                                              $request_date = $stat->requestId->requestDate;
                                                              $arr_date = explode("-", $request_date);
                                                              $date= $arr_date[2];
                                                              $month = $arr_date[1];
                                                              $year = $arr_date[0];
                                                              switch ($month){
                                                                  case 1 : $month ='Jan'; break;
                                                                  case 2 : $month ='Feb'; break;
                                                                  case 3 : $month = 'Mar'; break;
                                                                  case 4 : $month = 'Apr'; break;
                                                                  case 5 : $month = 'May'; break;
                                                                  case 6 : $month = 'Jun'; break;
                                                                  case 7 : $month = 'Jul'; break;
                                                                  case 8 : $month = 'Aug'; break;
                                                                  case 9 : $month = 'Sept'; break;
                                                                  case 10 : $month = 'Oct'; break;
                                                                  case 11 : $month = 'Nov'; break;
                                                                  case 12 : $month = 'Dec'; break;
                                                              }
                                                               if ($request_date == null){
                                                                  echo "-"; echo "<br/>";
                                                               }
                                                               else {
                                                                  echo "Date       : " .$date.' '.$month.' '.$year; echo "<br/>";
                                                               }

                                                                $status = $stat->requestId->status;
                                                               if ($status == "Pending"){
                                                               ?>
                                                                  <i class="text-danger"> Status : <?php echo $status; ?> </i> <br/>
                                                               <?php
                                                               }
                                                               else {
                                                               ?>
                                                                  <i class="text-info"> Status : <?php echo $status; ?> </i> <br/>
                                                               <?php
                                                               }
                                                               echo "Requestor  : " .$stat->requestId->requestor->userName; echo "<br/>";
                                                          ?>
                                                      </td>

                                                      <?php 
                                                        foreach ($request_status->requestStatus as $stat){
                                                          $status = $stat->approvalId->status;
                                                          if ($stat->approvalId->approvalId == $approval_id){
                                                               if ($app_user->status == "pending"){  
                                                      ?>  
                                                                <td>
                                                                   <?php
                                                                       $url = site_url('profile');
                                                                                $redirect = ltrim($url,"http://");
                                                                    ?>

                                                                    <form action="<?php echo   $this->auth_lib->url().'/submit/approval/'.$redirect; ?>" method="post" id="approval" >
                                                                        <div class="form-group">
                                                                            <label  class=" control-label">
                                                                            Comment (Optional) : <br/>
                                                                              <strong class="text-info">
                                                                                  NOTES! Jika pada tahap akhir, <br/> 
                                                                                  jika YES maka WAJIB mencantumkan tanggal <br/> 
                                                                                  dengan FORMAT 2015/12/01 <br/>  sebagai EXPIRED DATE
                                                                              </strong>
                                                                            </label>
                                                                            <input type="text"  class="form-control col-sm-2" name="content" id="content" >
                                                                        </div>

                                                                        <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
                                                                        <input type="hidden" id="approvalId" name="approvalId" value="<?php echo $stat->approvalId->approvalId; ?>">
                                                                        <input type="hidden" id="requestId.requestId" name="requestId.requestId" value="<?php echo $stat->requestId->requestId; ?>">
                                                                        <input type="hidden" id="requestTypeStep.reqTypeStepId" name="requestTypeStep.reqTypeStepId" value="<?php echo $stat->requestTypeStep->reqTypeStepId; ?>">
                                                                        <input type="hidden" id="target.userId" name="target.userId" value="<?php echo $stat->approvalId->target->userId; ?>">
                                                                         
                                                                        <button type="submit" name="status" value="Yes" class="btn btn-primary"  style=" margin-top: 7px;">
                                                                          Yes
                                                                        </button> 

                                                                        <button type="submit" name="status" value="No" class="btn btn-primary" style=" margin-top: 7px;" >
                                                                          No
                                                                        </button>     
                                                                  </form> 
                                                                </td>
                                                      <?php 
                                                                } //end cek status
                                                              else {
                                                      ?>
                                                              <!-- tampil list status approval based approval id -->
                                                              <td>
                                                                <?php 
                                                                   
                                                                    $approval_date = $app_user->approvalDate;
                                                                    if ($approval_date == null){
                                                                      echo "Approval Date   :  -"; echo "<br/>"; 
                                                                    }   
                                                                    else {
                                                                        $arr_date = explode("-", $approval_date);
                                                                        $date= $arr_date[2];
                                                                        $month = $arr_date[1];
                                                                        $year = $arr_date[0];
                                                                        switch ($month){
                                                                            case 1 : $month ='Jan'; break;
                                                                            case 2 : $month ='Feb'; break;
                                                                            case 3 : $month = 'Mar'; break;
                                                                            case 4 : $month = 'Apr'; break;
                                                                            case 5 : $month = 'May'; break;
                                                                            case 6 : $month = 'Jun'; break;
                                                                            case 7 : $month = 'Jul'; break;
                                                                            case 8 : $month = 'Aug'; break;
                                                                            case 9 : $month = 'Sept'; break;
                                                                            case 10 : $month = 'Oct'; break;
                                                                            case 11 : $month = 'Nov'; break;
                                                                            case 12 : $month = 'Dec'; break;
                                                                        }
                                                                         echo "Approval Date   : ". $date.' '.$month.' '.$year; echo "<br/>"; 
                                                                    }
                                                                     $status =  $app_user->status;
                                                                     if ($status == "Yes"){
                                                                     ?>
                                                                        <i class="text-info"> Status : <?php echo $status; ?> </i> <br/>
                                                                     <?php
                                                                     }
                                                                     else {
                                                                     ?>
                                                                        <i class="text-danger"> Status : <?php echo $status; ?> </i> <br/>
                                                                     <?php
                                                                     }
                                                                    
                                                                    echo "Content    : ".   $app_user->content; echo "<br/>";

                                                                ?>
                                                              </td>
                                                      <?php   }  // end else cek status ?>    
                                                      <?php
                                                          } // end cek approval id 
                                                          else {
                                                      ?>
                                                          <td>
                                                            <!-- tampil list2 -->
                                                            <?php 
                                                                
                                                                $approval_date = $stat->approvalId->approvalDate;
                                                                if ($approval_date == null){
                                                                  echo "Approval Date   :  -"; echo "<br/>"; 
                                                                } 
                                                                else {
                                                                    $arr_date = explode("-", $approval_date);
                                                                    $date= $arr_date[2];
                                                                    $month = $arr_date[1];
                                                                    $year = $arr_date[0];
                                                                    switch ($month){
                                                                        case 1 : $month ='Jan'; break;
                                                                        case 2 : $month ='Feb'; break;
                                                                        case 3 : $month = 'Mar'; break;
                                                                        case 4 : $month = 'Apr'; break;
                                                                        case 5 : $month = 'May'; break;
                                                                        case 6 : $month = 'Jun'; break;
                                                                        case 7 : $month = 'Jul'; break;
                                                                        case 8 : $month = 'Aug'; break;
                                                                        case 9 : $month = 'Sept'; break;
                                                                        case 10 : $month = 'Oct'; break;
                                                                        case 11 : $month = 'Nov'; break;
                                                                        case 12 : $month = 'Dec'; break;
                                                                    }
                                                                     echo "Approval Date   : ". $date.' '.$month.' '.$year; echo "<br/>"; 
                                                                }
                                                                 
                                                                $status =  $stat->approvalId->status;
                                                                 if ($status == "Yes"){
                                                                 ?>
                                                                    <i class="text-info"> Status : <?php echo $status; ?> </i> <br/>
                                                                 <?php
                                                                 }
                                                                 else {
                                                                 ?>
                                                                    <i class="text-danger"> Status : <?php echo $status; ?> </i> <br/>
                                                                 <?php
                                                                 }
                                                                
                                                                echo "Content    : ".  $stat->approvalId->content; echo "<br/>";
                                                            ?>
                                                          </td>
                                                      <?php
                                                           } // end else cek approval_id 
                                                         } // end foreach request status
                                                      ?>
                                                </tr>
                                            </tbody>

                                    </table>
                                      <?php 
                                        $cek_dokumen = $this->auth_lib->get_document_user($request_id);
                                        if (empty($cek_dokumen->docUserRoles)){

                                        }
                                        else { 
                                    ?>
                                         <a href="<?php echo site_url('profile/transmittal/'.$request_id) ?>" class="btn btn-primary"> 
                                          <i class="icon-docs"></i> Transmittal Letter
                                         </a>
                                    <?php
                                        } // end else cek dokumen
                                    ?>
                        <?php 
                                  } // end foreach $req = request_detail
                        ?>
                               <hr/>
                        <?php
                               } // end if $request_id

                             } // end foreach $approval_user
                       ?>
                           
                       <?php    
                          } // end if approval_user 
                          else {

                          }
                       ?>
                       <!-- end list approval notification -->
                     
                    </div>
                    <!-- END PANEL TAB #2 --> 
                    
                    <!-- PANEL TAB #3 -->   
                    <div class="tab-pane fade" id="tab3">
                        <?php 
                          if ($request_user){
                        ?>
                        <?php
                            foreach ($request_user->requests as $req_user){
                              $request_id = $req_user->requestId; 
                              $request_status = $this->auth_lib->get_request_status($request_id);
                              if ($request_id){
                                 // show request list 
                                 $request_detail =  $this->auth_lib->get_request_detail($request_id);
                        ?>
                        <?php
                                 // declare $request_detail into array
                                  $arr = array();
                                  $arr[] = $request_detail;
                                 /* foreach ($arr as $det){*/
                                    // get information request status
                                    $doc_user = $this->auth_lib->get_document_user($request_id);
                                    if (empty($doc_user->docUserRoles)){
                                       
                                    }
                                    else{
                                        // declare $request_detail into array
                                        $arr = array();
                                        $arr[] = $request_detail;
                                        foreach ($arr as $det){
                                        // get information request status
                                            $doc_request = $this->auth_lib->get_request_document($request_id);
                        ?>  
                                     <h4 class="text-info">    <strong>Transaction ID : <?php echo $request_id; ?> </strong> </h4>
                                         <div class="col-md-offset-1">
                                          <ul>
                                              <li > Document Request : </li>
                                                <div class="col-md-offset-1">
                        <?php               foreach ($doc_request->documentsByRequest as $data){ 
                        ?>                      
                                                   <li> 
                                                        <a href ="<?php echo site_url('profile/document/'.$data->documentId->documentId) ?>" >
                                                             <?php  echo $data->documentId->documentName."<br/>";  ?> 
                                                        </a> 
                                                    </li>
                                          
                        <?php
                                            } // end foreach doc_request
                        ?>        
                                                    </div>
                                              <?php foreach ($doc_user->docUserRoles as $dt){ } 
                                                        $start_date = explode("-", $dt->startDate);
                                                        $date= $start_date[2];
                                                        $month = $start_date[1];
                                                        $year = $start_date[0];
                                                        switch ($month){
                                                            case 1 : $month ='Jan'; break;
                                                            case 2 : $month ='Feb'; break;
                                                            case 3 : $month = 'Mar'; break;
                                                            case 4 : $month = 'Apr'; break;
                                                            case 5 : $month = 'May'; break;
                                                            case 6 : $month = 'Jun'; break;
                                                            case 7 : $month = 'Jul'; break;
                                                            case 8 : $month = 'Aug'; break;
                                                            case 9 : $month = 'Sept'; break;
                                                            case 10 : $month = 'Oct'; break;
                                                            case 11 : $month = 'Nov'; break;
                                                            case 12 : $month = 'Dec'; break;
                                                        }
                                                     
                                                        $expiry_date = explode("-", $dt->expiryDate);
                                                        $date_expiry= $expiry_date[2];
                                                        $month_expiry = $expiry_date[1];
                                                        $year_expiry = $expiry_date[0];
                                                        switch ($month_expiry){
                                                            case 1 : $month_expiry ='Jan'; break;
                                                            case 2 : $month_expiry ='Feb'; break;
                                                            case 3 : $month_expiry = 'Mar'; break;
                                                            case 4 : $month_expiry = 'Apr'; break;
                                                            case 5 : $month_expiry = 'May'; break;
                                                            case 6 : $month_expiry = 'Jun'; break;
                                                            case 7 : $month_expiry = 'Jul'; break;
                                                            case 8 : $month_expiry = 'Aug'; break;
                                                            case 9 : $month_expiry = 'Sept'; break;
                                                            case 10 : $month_expiry = 'Oct'; break;
                                                            case 11 : $month_expiry = 'Nov'; break;
                                                            case 12 : $month_expiry = 'Dec'; break;
                                                        }
                                                ?>
                                              <li> Start Date : <?php echo $date.' '.$month.' '.$year; ?> </li>
                                              <li> Expiry Date : <?php echo $date_expiry.' '.$month_expiry.' '.$year_expiry; ?> </li>
                                              <li> Document Owner : <?php echo $data->documentId->businessAssociate->baName; ?> </li>
                                          </ul>
                                        </div>
                                        <hr/>
                        <?php
                                      } // end foreach detail request
                                    }
                        ?>

                        <?php 

                             /* } // end foreach request detail*/
                            } // end if request id
                          } // end foreach request_user 
                        } // end if request_user
                        ?>
                    </div>
                     <!-- END PANEL TAB #3 --> 
                </div>
             </div>
             <!-- END PANEL CONTENT -->
         </div>
      <!-- END OF PANEL -->
    </div>
</div>


