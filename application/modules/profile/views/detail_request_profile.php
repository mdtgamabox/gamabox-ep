<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Profile</strong> View</h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('') ?>">Home</a>
      </li>
      <li class="active">Profile</li>
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<!-- PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
     <!-- README PANEL -->
         <div class="panel panel-main">
            <div class="panel-header panel-controls">
              <h3><i class="icon-layers"></i><strong>Profile</strong> Page</h3>
            </div>

            <!-- PANEL CONTENT -->
             <div class="panel-content content-profile">
                <?php foreach ($avatar->avatarUser as $data) { ?>
                <h2><strong><?php echo ucfirst($data->userId->userName); ?></strong> | Profile ID</h2>
                <?php } ?>
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
                  <li><a href="#tab2" data-toggle="tab"><i class="icon-docs"></i>Transaction</a></li>
                </ul>

                <div class="tab-content tab-head">
                     <!-- PANEL TAB #1 -->               
                    <div class="tab-pane fade active in" id="tab1">
                        <h4> <strong class="text-info">User Profile</strong> </h4>
                        <?php foreach ($avatar->avatarUser as $data){ ?>
                        <div class="row">
                          <div class="col-md-8">
                               <table class="table table-striped">
                                  <tr> 
                                      <td> <strong > Username &nbsp;</strong> </td>
                                      <td> &nbsp; <?php echo $data->userId->userName; ?> </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Email &nbsp; </strong> </td>
                                      <td> &nbsp; <?php echo $data->userId->userEmail; ?> </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Business Associate &nbsp; </strong>  </td>
                                      <td>  &nbsp; <?php echo $data->userId->businessAssociate->baName; ?>  </td>
                                  </tr>
                                  <tr>
                                      <td> <strong >Business Associate Type &nbsp; </strong>  </td>
                                      <td>  &nbsp; <?php echo $data->userId->businessAssociate->baType; ?>  </td>
                                  </tr>
                              </table>
                          </div>              
                        </div>
                        <?php } ?>
                    </div>
                    <!-- END PANEL TAB #1 -->

                    <!-- PANEL TAB #2 -->   
                    <div class="tab-pane fade" id="tab2">
                        <!-- display request based on request_id -->
                        <?php 
                           // $request_detail = $this->auth_lib->get_request_detail($request_id);
                            if ($request_detail){ 
                             
                          ?>

                          <!-- get request based on request_id -->
                        <?php
                         // declare $request_detail into array
                          $arr = array();
                          $arr[] = $request_detail;
                          
                           foreach ($arr as $det){
                              $type_id = $det->requestType->reqTypeId;
                              $step_detail = $this->auth_lib->get_request_step($type_id);
                              $doc_request = $this->auth_lib->get_request_document($request_id);
                         ?>
                      <h4 class="text-info"> <strong > Request Detail Information </strong> </h4>
                      <ul>
                          <li > Document Request : </li>
                              <div class="col-md-offset-1">
                      <?php   
                      foreach ($doc_request->documentsByRequest as $data){ 
                      ?>
                             
                             <ul>
                                <li> <?php echo $data->documentId->documentName."<br/>";  ?></li>
                             </ul>
                    <?php
                      } // end foreach doc_request
                      ?>                
                            </div>
                          <li> BA Name Target : <?php echo $data->documentId->businessAssociate->baName; ?> </li>
                    </ul>
                        <table class="table table-hover">
                          <thead>
                           <tr>
                              <?php
                                  $step_detail = $this->auth_lib->get_request_step($type_id);                                   
                                  foreach ($step_detail->requestTypeSteps as $val){
                               
                              ?>
                              <th> <?php  echo $val->stepName; ?> </th>
                                    
                             <?php 
                                // end foreach step_detail
                                }
                             ?>
                            </tr>
                          </thead>

                          
                        <tbody>
                            <tr>
                                <?php 
                                     foreach ($request_status->requestStatus as $stat){
                                       }
                                ?>
                                  <td> 
                                    <?php
                                           $request_date = $stat->requestId->requestDate;
                                            $arr_date = explode("-", $request_date);
                                            $date= $arr_date[2];
                                            $month = $arr_date[1];
                                            $year = $arr_date[0];
                                            switch ($month){
                                                case 1 : $month ='Jan'; break;
                                                case 2 : $month ='Feb'; break;
                                                case 3 : $month = 'Mar'; break;
                                                case 4 : $month = 'Apr'; break;
                                                case 5 : $month = 'May'; break;
                                                case 6 : $month = 'Jun'; break;
                                                case 7 : $month = 'Jul'; break;
                                                case 8 : $month = 'Aug'; break;
                                                case 9 : $month = 'Sept'; break;
                                                case 10 : $month = 'Oct'; break;
                                                case 11 : $month = 'Nov'; break;
                                                case 12 : $month = 'Dec'; break;
                                            }
                                             if ($request_date == null){
                                                echo "-";
                                             }
                                             else {
                                                echo "Date       : " .$date.' '.$month.' '.$year; echo "<br/>";
                                             }
                                         echo "Status     : " .$stat->requestId->status; echo "<br/>";
                                         echo "Requestor  : " .$stat->requestId->requestor->userName; echo "<br/>";
                                     ?>
                                  </td>

                                <?php
                                    // matching type in detail and status
                                    $step = $this->auth_lib->get_request_step($type_id);
                                    foreach ($step->requestTypeSteps as $val){
                                        $a = $val->reqTypeStepId;   
                                    }
                                    foreach ($request_status->requestStatus as $stat){
                                ?>    
                                  <td>
                                      <?php 
                                            
                                              $approval_date = $stat->approvalId->approvalDate;
                                              if ($approval_date){
                                                $arr_date = explode("-", $approval_date);
                                                $date= $arr_date[2];
                                                $month = $arr_date[1];
                                                $year = $arr_date[0];
                                                switch ($month){
                                                    case 1 : $month ='Jan'; break;
                                                    case 2 : $month ='Feb'; break;
                                                    case 3 : $month = 'Mar'; break;
                                                    case 4 : $month = 'Apr'; break;
                                                    case 5 : $month = 'May'; break;
                                                    case 6 : $month = 'Jun'; break;
                                                    case 7 : $month = 'Jul'; break;
                                                    case 8 : $month = 'Aug'; break;
                                                    case 9 : $month = 'Sept'; break;
                                                    case 10 : $month = 'Oct'; break;
                                                    case 11 : $month = 'Nov'; break;
                                                    case 12 : $month = 'Dec'; break;
                                                }
                                                echo "Date       : " .$date.' '.$month.' '.$year; echo "<br/>";
                                              }
                                              else{
                                                echo "Date        : - "; echo "<br/>";
                                              }
                                            echo  "Status : " .$stat->approvalId->status; echo "<br/>";
                                            echo  "Target : " .$stat->approvalId->target->userName; echo "<br/>";
                                      ?>
                                  </td>
                            <?php
                                } 
                            ?>
                            </tr>
                        </tbody>
                              <?php 
                                // end foreach request_detail
                                 }
                               }
                              ?>
                        </table>
                       
                    </div>
                    <!-- END PANEL TAB #2 -->   
                </div>
             </div>
             <!-- END PANEL CONTENT -->
         </div>
      <!-- END OF PANEL -->
    </div>
</div>