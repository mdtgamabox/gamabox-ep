<?php
	class Profile extends MX_Controller{
		function __construct(){
			parent::__construct();

			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('date','form', 'url', 'file'));

		}	

		function index(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$request_id = $this->uri->segment('3');
				$data['title'] 		=  "Profile";
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
					
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart']	= $update_cart;

				
				// $data['request_detail'] =  $this->auth_lib->get_request_detail($request_id);
				// $data['request_status'] =  $this->auth_lib->get_request_status($request_id);
				$data['request_user'] 	= $this->auth_lib->get_request_user($session);
				$data['approval_user']  = $this->auth_lib->get_approval_user($session);

				// check role
				$role = $this->session->userdata('role');
				$a = str_replace(",%20", " ", $role);
				$role_array = explode(" ", $a);
				//echo $role_array[0];
				if ($role_array[0] == "ROLE_PD"){
					$data['request_user'] 	= null;
					$data['approval_user']  = $this->auth_lib->get_approval_user($session);
				}
				else {
					$data['request_user'] 	= $this->auth_lib->get_request_user($session);
					$data['approval_user']  = null;
				}

				$data['request_id']		= $request_id;
				$data['content'] 		= $this->load->view('home_profile', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
							
			}
			else{
				redirect('');
			}

		}

		function request_detail(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$request_id = $this->uri->segment('3');
				$data['title'] 		=  "Profile";
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
					
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart']	= $update_cart;

				/*get transaction information*/
				$data['request_detail'] = $this->auth_lib->get_request_detail($request_id);
				$data['request_status'] = $this->auth_lib->get_request_status($request_id);
				$data['request_user'] 	= $this->auth_lib->get_request_user($session);
				$data['approval_user']  = $this->auth_lib->get_approval_user($session);
				
				
				$data['request_id']		= $request_id;
				$data['user_id']		= $session;

				$data['content'] 		= $this->load->view('detail_request_profile', $data, TRUE);

				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}
		}




		function well(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$uwi = $this->uri->segment(3);
				$data['uwi'] = $uwi;
				$data['title'] 		=  "Well | ".$uwi ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('well', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}

		function field(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$fieldId = $this->uri->segment(3);
				$data['fieldId'] = $fieldId;
				$data['title'] 		=  "Field | ".$fieldId ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('field', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}

		function area(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$areaId = $this->uri->segment(3);
				$data['areaId'] = $areaId;
				$data['title'] 		=  "Area | ".$areaId ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('area', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}

		function warehouse(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$warehouseId = $this->uri->segment(3);
				$data['warehouseId'] = $warehouseId;
				$data['title'] 		=  "Warehouse | ".$warehouseId ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('warehouse', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}

		function document(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$documentId = $this->uri->segment(3);
				$data['documentId'] = $documentId;
				$data['title'] 		=  "Document | ".$documentId ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('document', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}
		function seismic(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$seismicId = $this->uri->segment(3);
				$seismicType = $this->uri->segment(4);
				$data['seismicId'] = $seismicId;
				$data['seismicType'] = $seismicType;
				$data['title'] 		=  "Seismic | ".$seismicId." ".$seismicType  ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('seismic', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}
		function notification(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
								
				$data['title'] 		=  "Notification"  ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('notification', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_notification', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}
		function transmittal(){

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$requestId = $this->uri->segment(3);
				$data['requestId'] = $requestId;
				$data['title'] 		=  "Transmittal | ".$requestId ;
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('transmittal', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_profile', $data, TRUE);
				$data['content_js']		= $this->load->view('js_profile', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}

		}
	}
?>