<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Tree</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Tree View</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
	<!-- BEGIN LIST OF TREE VIEW -->
	<div class="col-md-2 side-treeview">
    <h3><strong>Area</strong> Pertamina UTC</h3>
    <div id="tree-area"></div>
    <hr>
    <h3><strong>Seismic</strong> </h3>
    <div id="tree-seismic"></div>
    <hr>
    <h3><strong>Warehouse</strong> Data</h3>
    <div id="tree-warehouse"></div>
    <hr>
    <h3><strong>Well</strong> List</h3>
    <div id="tree-well"></div>
    <hr>
  </div>
  <!-- END LIST OF TREE VIEW -->
  <div class="col-md-10">
    <!-- README PANEL FIELD -->
    <div class="panel panel-main" id="panel-field">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Field</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleField"><strong>Nama Field</strong> | Field ID</h2>
      </div>
    </div>
    <!-- README PANEL SEISMIC -->
    <div class="panel panel-main" id="panel-seismic">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Seismic</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleSeismic"><strong>Nama Seismic</strong> | Seismic ID</h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#seismic-tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
          <li><a href="#seismic-tab2" data-toggle="tab"><i class="icon-eye"></i>Coordinate</a></li>
          <li><a href="#seismic-tab3" data-toggle="tab"><i class="icon-doc"></i>Documents</a></li>
          <li><a href="#seismic-tab4" data-toggle="tab"><i class="icon-docs"></i>Seismic Data</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="seismic-tab1">
            
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="seismic-tab2">
          </div>
          <!-- README TAB 3 -->
          <div class="tab-pane fade" id="seismic-tab3">
          </div>
          <!-- README TAB 4 -->
          <div class="tab-pane fade" id="seismic-tab4">
          </div>
        </div>
      </div>  
    </div>
    <!-- README PANEL AREA -->
    <div class="panel panel-main" id="panel-area">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Area</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleArea"><strong>Nama Area</strong> | Area ID</h2>
      </div>
    </div>
    <!-- README PANEL WAREHOUSE -->
    <div class="panel panel-main" id="panel-warehouse">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Warehouse</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleWarehouse"><strong>Nama Warehouse</strong> | Warehouse ID</h2>
      </div>
    </div>
    <!-- README PANEL WELL -->
              <div class="panel panel-main" id="panel-well">
                <div class="panel-header panel-controls">
                  <h3><i class="icon-layers"></i> <strong>Well</strong> Profile<div id="properties"></div></h3>
                </div>
                <div class="panel-content content-profile" id="container-content">
                  <h2 id="TitleWell"> </h2>
                    <!-- FIXME Konten isi Tab Well -->
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
                      <li><a href="#tab2" data-toggle="tab"><i class="icon-docs"></i>G &amp; G Doc</a></li>
                      <li><a href="#tab3" data-toggle="tab"><i class="icon-speedometer"></i> Production</a></li>
                      <li><a href="#tab4" data-toggle="tab"><i class="icon-globe"></i> Other Report</a></li>
                    </ul>
                    <div class="tab-content tab-head">
                      <!-- README Konten Tab 1 Information -->
                      <div class="tab-pane fade active in" id="tab1">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1_1" data-toggle="tab">Basic Information</a></li>
                            <li class=""><a href="#tab1_2" data-toggle="tab">Map</a></li>
                            <li><a href="#tab1_3" data-toggle="tab">Well History / Profile</a></li>
                          </ul>
                          <div class="tab-content">
                            <!-- README Konten Tab 1 1 Basic Information-->
                            <div class="tab-pane fade active in" id="tab1_1">
                            </div>
                            <!-- README Konten Tab 1 2 Map -->
                            <div class="tab-pane fade" id="tab1_2">
                            </div>
                            <!-- README Konten Tab 1 3 Well History / Profile-->
                            <div class="tab-pane fade" id="tab1_3">
                              <div class="row">
                                <div class="col-md-4">
                                  <h3 class="text-center">Well Profile</h3>
                                  <div id="avatar-well">
                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <h3 class="text-center">Well History</h3>
                                  <div class="table-responsive">
                                    <div id="table-wellactivityx">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <!-- README Konten Tab 2 -->
                      <div class="tab-pane fade" id="tab2">
                        <div class="row">
                          <div class="col-md-2">
                            <div id="tree-doctype-well" class="side-treeview"></div>  
                          </div>
                          <div class="col-md-10">
                            <!-- TABLE DOC WELL -->
                            <div class="table-responsive">
                              <div id="table-welldocument">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- README Konten Tab 3 -->
                      <div class="tab-pane fade" id="tab3">
                        <div class="row">
                          <div class="col-md-12 widget-full" id="container-graph">
                            <div id="chart-wellproduction"></div>    
                            <div class="table-responsive" id="table-wellproduction-container"></div>
                          </div>
                        </div>
                      </div>
                      <!-- README Konten Tab 4 -->
                      <div class="tab-pane fade" id="tab4">
                        <p>Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                      </div>
                    </div>
                </div>
              </div>
            </div>
</div>

 