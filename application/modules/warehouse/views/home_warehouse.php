
 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Warehouse</strong> View</h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('') ?>">Home</a>
      </li>
      <li class="active">Warehouse</li>
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->


<!-- PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
     <!-- README PANEL -->
         <div class="panel panel-main">
            <div class="panel-header panel-controls">
              <h3><i class="icon-folder-alt"></i><strong>Warehouse</strong> Page</h3>
            </div>

            <!-- PANEL CONTENT -->
             <div class="panel-content ">
             	<table class="table table-dynamic table-tools">
             		<thead>
             			<tr>
             				<td> No. </td>
             				<td> Warehouse ID</td>
             				<td> Warehouse Group<td>
             			</tr>
             		</thead>
             			<tbody>
		             		<?php      
		             			$no = $this->uri->segment(3)+1 ;       			
								foreach ($warehouse as $data){
									
									foreach ($data as $val){
							?>
										<tr>
											<td> <?php echo $no; $no = $no+1;?> </td>										
											<td> <a href="<?php echo site_url('warehouse/document/'.$val->warehouseDetailId) ?>"><?php echo $val->warehouseDetailId ?> </a></td>
											<td><?php echo $val->warehouseName  ?> </td>
										</tr>
							<?php
									
										
									}
									
								}
							 ?>
		             	</tbody>
             	</table>

             
                        		
             </div>
             <!-- end panel content -->
         </div>
     </div>
</div>