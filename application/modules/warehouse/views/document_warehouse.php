 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Warehouse Document</strong> View</h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('') ?>">Home</a>
      </li>
      <li > <a href="<?php echo site_url('warehouse') ?>"> Warehouse</a> </li>
      <li class="active">Warehouse Document</li>
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<!-- PAGE CONTENT -->
<div class="row">
    <div class="col-md-12">
     <!-- README PANEL -->
         <div class="panel panel-main">
            <div class="panel-header panel-controls">
              <h3><i class="icon-folder-alt"></i><strong>Warehouse Document</strong> Page</h3>
            </div>

            <!-- PANEL CONTENT -->
             <div class="panel-content">
             	<table class="table table-dynamic table-tools">
             		<thead>
             			<tr>
             				<td> No </td>
             				<td> Document Title</td>
             				<td> Owner</td>
             				<td> Physical Data</td>
             				<td> Digital Data</td>
             				<td> Storage Detail</td>
             				<td> Status</td>
             				<td> Stock</td>
             			</tr>
             		</thead>

             		<tbody>
             			<?php
		             		$no = $this->uri->segment(4)+1 ; 
		             		foreach ($warehouse_doc as $data){
		             			foreach ($data as $value){
		             	?>
		             				<tr> 
		             					<td> <?php echo $no; $no = $no+1;?>  </td>
		             					<td> <?php echo ucfirst($value->documentName); ?></td>
		             					<td> <?php echo $value->businessAssociate->baName." (".$value->businessAssociate->baType .")"?></td>
		             					<td> <?php if ($value->isPhysical == 0){echo "No";} else{echo "Yes";} ?> </td>
		             					<td> <?php if ($value->isDigital == 0){echo "No";} else{echo "Yes";} ?> </td>
		             					<td>
		             						 <?php 
		             						 	echo "<ul>";		             						 		
		             						 		echo "<li> ID: ". $value->storageId->warehouseId.", Type : ".$value->storageId->warehouseType."</li>";
		             						 		if (isset($value->storageId->warehouseGroup->warehouseId)){
		             						 			echo "<li> ID: ".$value->storageId->warehouseGroup->warehouseId.", Type : ".$value->storageId->warehouseGroup->warehouseType."</li>";
		             						 		}
		             						 		else {echo " ";}
		             						 		if (isset($value->storageId->warehouseGroup->warehouseGroup->warehouseId)){
		             						 			echo "<li> ID: ".$value->storageId->warehouseGroup->warehouseGroup->warehouseId.", Type : ".$value->storageId->warehouseGroup->warehouseGroup->warehouseType."</li>";
		             						 		}
		             						 		else {echo " ";}
		             						 		if (isset($value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseId)){
		             						 			echo "<li> ID: ".$value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseId.", Type : ".$value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseType."</li>";
		             						 		}
		             						 		else {echo " ";}
		             						 		if (isset($value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseGroup->warehouseId)){
		             						 			echo "<li> ID: ".$value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseGroup->warehouseId.", Type : ".$value->storageId->warehouseGroup->warehouseGroup->warehouseGroup->warehouseGroup->warehouseType."</li>";
		             						 		}
		             						 		else {echo " ";}
		             						 	echo "</ul>";
		             						 ?>
		             					</td>
		             					<td> 
		             						<?php 
		             							if ($value->stock == NULL) {
		             								echo "Empty";
		             							} 
		             							else {
		             								echo "Available";
		             							}
		             						?> 
		             					</td>
		             					<td> <?php echo $value->stock ?> </td>
		             				</tr>
		             	<?php
		             				
		             			}
		             		}
		             	?>
             		</tbody>
             	</table>

             </div>
            <!-- END PANEL CONTENT -->
        </div>
    </div>
</div>