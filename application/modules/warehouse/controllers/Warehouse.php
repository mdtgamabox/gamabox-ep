<?php 
	Class Warehouse extends MX_Controller{
		function __construct(){
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			 if (!$session  && !$role){
	            redirect (''); 
	        }
		}


		function index(){
			$data['title'] 		=  "Warehouse";
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			/*insert to auth_lib as session_id*/
			$data['profile'] 	= $this->auth_lib->profile($session);
			$data['avatar']		= $this->auth_lib->avatar($session);
			$data['document']	= $this->auth_lib->get_document($session);

			/*get total cart session*/
			$total_cart = array();
			$total_cart[]	= $this->session->userdata('int_aph_cart');
			$total_cart[]	= $this->session->userdata('corporate_cart');
			$total_cart[] 	= $this->session->userdata('ext_aph_cart');
			$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
			$update_cart = array();
			foreach($total_cart as $arrs) {
			    if(is_array($arrs)) {
			        $update_cart = array_merge($update_cart, $arrs);
			    }
			}
			$data['update_cart'] 	= $update_cart;
			$data['warehouse']		= $this->auth_lib->get_warehouse_detail();
			$data['content'] 		= $this->load->view('home_warehouse', $data, TRUE);
			$data['content_css'] 	= $this->load->view('css_warehouse', $data, TRUE);
			$data['content_js']		= $this->load->view('js_warehouse', $data, TRUE);
			$this->load->view('frame-template',$data);
		}

		function document($warhouse_id){
			$data['title'] 		=  "Warehouse Document";
			$warehouse_id = $this->uri->segment(3);
			$session = $this->session->userdata('session');
			$role = $this->session->userdata('role');
				/*insert to auth_lib as session_id*/
			$data['profile'] 	= $this->auth_lib->profile($session);
			$data['avatar']		= $this->auth_lib->avatar($session);
			$data['document']	= $this->auth_lib->get_document($session);

			/*get total cart session*/
			$total_cart = array();
			$total_cart[]	= $this->session->userdata('int_aph_cart');
			$total_cart[]	= $this->session->userdata('corporate_cart');
			$total_cart[] 	= $this->session->userdata('ext_aph_cart');
			$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
			$update_cart = array();
			foreach($total_cart as $arrs) {
			    if(is_array($arrs)) {
			        $update_cart = array_merge($update_cart, $arrs);
			    }
			}
			$data['update_cart'] 	= $update_cart;
			$data['warehouse_doc'] = $this->auth_lib->get_warehouse_document($warehouse_id);
			$data['content']	 	= $this->load->view('document_warehouse', $data, TRUE);
			$data['content_css'] 	= $this->load->view('css_warehouse', $data, TRUE);
			$data['content_js']		= $this->load->view('js_warehouse', $data, TRUE);
			$this->load->view('frame-template',$data);
		}
	}

?>