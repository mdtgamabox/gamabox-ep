<?php 
	/*showing role from session*/
	$role = $this->session->userdata('role');
		$a = str_replace(",%20", " ", $role);
		$role_array = explode(" ", $a);
		
		
 ?>

<div class="row">
	<div class="col-xlg-12 col-lg-12">
		<div class="row">
            <div class="col-md-12">
              <div class="panel widget-map">
                <div class="panel-content widget-full">
                  <div id="map-openlayer" class="map" style="height:100%"><div id="popup"></div></div>
                </div>
              </div>
            </div>
          </div>
	</div>
	<div class="col-xlg-6 col-lg-6">
		<div class="row">
			<div class="col-md-12">
				<div class="panel no-bd bd-3 panel-stat">
					<div class="panel-header">
                      	<h3><i class="icon-graph"></i> <strong>Last 12 Months </strong> Production</h3>
                     	<div class="control-btn">
                        	<a href="#" class="panel-reload hidden"><i class="icon-reload"></i></a>
                      	</div>
                    </div>
                    <div class="panel-body p-15 p-b-0">
                    	<div class="row m-b-10">
                    		<div class="col-xs-3 big-icon">
	                          <i class="icon-graph"></i>
	                        </div>
	                        <div class="col-xs-9">
	                          	<div class="live-tile" data-mode="carousel" data-direction="vertical" data-delay="3500" data-height="60">
		                            <div>
		                              	<small class="stat-title">Oil (Bbl)</small>
		                              	<h1 class="f-40 m-0 w-300" id="jumlahOil">0</h1>
		                            </div>
		                            <div>
		                              	<small class="stat-title">Gas (MCFD)</small>
		                              	<h1 class="f-40 m-0 w-300" id="jumlahGas">0</h1>
		                            </div>
	                          </div>
	                        </div>
                    	</div>
                    </div>
                    <div class="panel-stat-chart">
                      	<canvas id="monthproduction-chart" class="full"></canvas>
                    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xlg-6 col-lg-6">
		<div class="row">
			<div class="col-md-12">
                <div class="panel no-bd bd-3 panel-stat">
                    <div class="panel-header">
                      	<h3><i class="icon-graph"></i> <strong>Warehouse</strong> Statistic</h3>
                      	<div class="control-btn">
                        	<a href="#" class="panel-reload hidden"><i class="icon-reload"></i></a>
                     	</div>
                    </div>
                    <div class="panel-body p-15 p-b-0">
                      	<div class="row m-b-10">
	                        <div class="col-xs-3 big-icon">
	                          	<i class="icon-pie-chart"></i>
	                        </div>
	                        <div class="col-xs-9">
	                          	<div class="live-tile" data-mode="carousel" data-direction="vertical" data-delay="3500" data-height="60">
		                            <div>
		                              	<small class="stat-title">Digital Data</small>
		                              	<h1 class="f-40 m-0 w-300">23523</h1>
		                            </div>
		                            <div>
		                              	<small class="stat-title">Physical Data</small>
		                              	<h1 class="f-40 m-0 w-300">34532</h1>
		                            </div>
	                          	</div>
	                        </div>
                     	 </div>
                    </div>
                    <div class="panel-stat-chart">
                      <canvas id="warehouse-chart"></canvas>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xlg-12">
	  	<div class="panel">
	   		<div class="panel-header panel-controls">
	      		<h3><i class="icon-graph"></i> <strong>Production</strong> Chart</h3>
	    	</div>
		    <div class="panel-content widget-full widget-stock stock1">
		      	<div id="production-chart"></div>
		    </div>
		</div>
	</div>
</div>