<?php
	class Dashboard extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
		}	


		function index(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*insert to auth_lib as session_id*/
				$data['profile']	= $this->auth_lib->profile($session);
				$data['avatar']		= $this->auth_lib->avatar($session);
				$data['url']		= $this->auth_lib->url();

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				
				$data['title']			= "Dashboard";
				$data['content']		= $this->load->view('dash', $data, TRUE);
				$data['content_js']		= $this->load->view('js_dashboard', $data, TRUE);
				$data['content_css']	= $this->load->view('css_dashboard', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else {
				redirect('');
			}
		}

		

		function logout() {
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
	            $this->session->sess_destroy();
	            $this->session->unset_userdata('int_aph_cart');
				$this->session->unset_userdata('corporate_cart');
				$this->session->unset_userdata('ext_aph_cart');
				$this->session->unset_userdata('ext_ba_cart');
				$this->session->unset_userdata('cor_to_aph_cart');
				
	            
	            redirect('');
	        }
	        else{
	        	redirect('');
	        }
        }

        function helpinfo(){
        	$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*insert to auth_lib as session_id*/
				$data['profile']	= $this->auth_lib->profile($session);
				$data['avatar']		= $this->auth_lib->avatar($session);
				$data['url']		= $this->auth_lib->url();

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				
				
				$data['title']			= "Dashboard";
				$data['content']		= $this->load->view('info_help', $data, TRUE);
				$data['content_js']		= '';
				$data['content_css']	= $this->load->view('css_dashboard', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else {
				redirect('');
			}
        }

         function termofuse(){
        	$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*insert to auth_lib as session_id*/
				$data['profile']	= $this->auth_lib->profile($session);
				$data['avatar']		= $this->auth_lib->avatar($session);
				$data['url']		= $this->auth_lib->url();

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				
				$data['title']			= "Dashboard";
				$data['content']		= $this->load->view('term_of_use', $data, TRUE);
				$data['content_js']		= '';
				$data['content_css']	= $this->load->view('css_dashboard', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else {
				redirect('');
			}
        }

	}

?>