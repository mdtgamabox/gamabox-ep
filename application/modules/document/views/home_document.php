<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-upload">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Documents</strong> </h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleUpload">List of <strong>Document</strong></h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#upload-tab1" data-toggle="tab"><i class="icon-docs"></i>Documents</a></li>
          <li><a href="#upload-tab2" data-toggle="tab"><i class="fa fa-bank"></i>Warehouse</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in">
            <div class="row">
              <div class="col-md-3">
                <div  id="tree-doctype">
                  
                </div>
              </div>
              <div class="col-md-9" >
                <div id="list-of-document">
                  
                </div>
              </div>  
            </div>
            
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="upload-tab2">
            <table>
                
            </table>
          </div>
        </div>
        <!-- END Tab  -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  
</script>

 