<?php
	class Document extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('date','form', 'url', 'file'));
		}	

		function index(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				
				$data['title'] 		=  "Document GAMABOX-EP";
				/*insert to auth_lib as session_id*/
				$data['profile']		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;
				$data['content'] 		= $this->load->view('home_document', $data, TRUE);
				$data['content_css'] 	= $this->load->view('css_document', $data, TRUE);
				$data['content_js']		= $this->load->view('js_document', $data, TRUE);
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}
		}

	
	}
?>