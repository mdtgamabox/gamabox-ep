<?php 
	Class Transaction extends MX_Controller{
		function __construct(){
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
		}

		function index(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$data['title'] = "Request Page";

				/*insert to auth_lib as session_id*/
				$data['profile'] 	= $this->auth_lib->profile($session);
				$data['avatar']		= $this->auth_lib->avatar($session);
				$data['document']	= $this->auth_lib->get_document($session);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content_js'] = " ";
				$data['content_css'] = " ";
				$data['content'] = $this->load->view('request', $data, TRUE);
			
				$this->load->view('frame-template',$data);
			}
			else{
				redirect('');
			}
		}

		
		function add_cart(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){

				/*get data unserialize*/
				$data = $_POST["document"];
				$returndata = array();
				$str_array = explode("&", $data);
				foreach ($str_array as $item) {
			        $array = explode("=", $item);
			        $returndata[$array[0]] = $array[1];
			    }

			    $baType_doc	= $returndata['baType'];
		  		$doc_id		= $returndata['documentId'];
		   		$ba_doc	= $returndata['businessAssociate']; 

				/*get user baType*/
				$profile = $this->auth_lib->profile($session);
				foreach ($profile->user as $data){
					$baType_user = $data->baType;
					$ba_user = $data->businessAssociate;
				}


				if ($baType_user == "KORPORAT"){
					// jika ba type user adalah korporat
					if ($baType_doc == $baType_user){
						// peminjaman ke satu korporat
						$this->add_corporate($baType_doc,$doc_id,$ba_doc);
					}
					else {
						// peminjaman dari korporat ke aph
						$this->add_cor_to_aph($baType_doc,$doc_id,$ba_doc);
					}
				}
				elseif ($baType_user == "APH"){
					// jika ba type user adalah aph
					if ($baType_doc == $baType_user){
						if ($ba_user == $ba_doc){
							/*peminjaman ke satu aph*/
							$this->add($baType_doc,$doc_id,$ba_doc);
						}
						else{
							// peminjaman antar aph
							$this->add_ext_aph($baType_doc,$doc_id,$ba_doc);
						}
												
					}
					else{
						// peminjaman aph ke korporat 
						$this->add_ext_aph($baType_doc,$doc_id,$ba_doc);
					}
				}
		
				

				}
			else{
				redirect('');
			}
		}


		function add($baType_doc,$doc_id,$ba_doc){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*menampung documentId ke dalam array, kemudian di detailkan dengan get detail_document nya*/
				$request_document = array();

				/*kondisi jika sudah ada data yang ditambahkan*/
				if ($this->session->userdata('int_aph_cart')){
					$int_aph_cart = $this->session->userdata('int_aph_cart');
					foreach ($int_aph_cart as $id=>$val){
						$request_document[$id] = $val;
					}
				}

				/*asumsi document yang boleh ditambahkan hanya satu*/
				$doc_add = 1;
				$request_document[$doc_id] = $doc_add;
				
				$this->session->set_userdata('int_aph_cart',$request_document);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				
				$arr = array();
				$arr['update_cart'] = array_sum($update_cart);

				echo json_encode($arr);
				// redirect('transaction');
			}
			else{
				redirect('');
			}
		}


		function add_corporate($baType_doc,$doc_id,$ba_doc){
			$session 	= $this->session->userdata('session');
			$role		= $this->session->userdata('role');

			if ($session != null && $role != null){
				$request_corporate = array();

				if ($this->session->userdata('corporate_cart')){
					$corporate_cart = $this->session->userdata('corporate_cart');
					foreach ($corporate_cart as $id=>$val){
						$request_corporate[$id] = $val;
					}
				}
				
				$doc_add = 1;
				$request_corporate[$doc_id] = $doc_add;

				$this->session->set_userdata('corporate_cart',$request_corporate);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();			

				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
			
				$arr = array();

				$arr['update_cart'] = array_sum($update_cart);
				echo json_encode($arr);
			}
			else{
				redirect('');
			}
		}

		function add_cor_to_aph($baType_doc,$doc_id,$ba_doc){
			$session = $this->session->userdata('session');
			$role	 = $this->session->userdata('role');

			if ($session != null && $role != null){
				$request_aph = array();
				$request_ba = array();
				$docid = array();

				if ($this->session->userdata('cor_to_aph_cart')){
					$cor_to_aph = $this->session->userdata('cor_to_aph_cart');

					foreach($cor_to_aph as $id=>$val){
						$request_aph[$id] = $val;
					}
				}


				$tmp = array();
				if ($this->session->userdata('ext_ba_cart')){
					$ba_aph_cart = $this->session->userdata('ext_ba_cart');
					foreach ($ba_aph_cart as $id=>$val){
							$request_ba[$id]= $val;
					}					
				}

				$request_ba[$ba_doc][] = $doc_id;
				
				$this->session->set_userdata('ext_ba_cart',$request_ba);

				$doc_add = 1;
				$request_aph[$doc_id] =$doc_add;

				$this->session->set_userdata('cor_to_aph_cart',$request_aph);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				
				$arr = array();
				
				$arr['update_cart'] = array_sum($update_cart);
				echo json_encode($arr);

			}

			else{
				redirect('');
			}
		}

		function add_ext_aph($baType_doc,$doc_id,$ba_doc){
			$session 	= $this->session->userdata('session');
			$role		= $this->session->userdata('role');

			if ($session != null && $role != null){
				$request_ext_aph = array();
				$request_ba = array();

				$docid = array();

				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach ($ext_aph_cart as $id=>$val){
						$request_ext_aph[$id] = $val;
					}
				}
				
				$tmp = array();
				if ($this->session->userdata('ext_ba_cart')){
					$ext_aph_cart = $this->session->userdata('ext_ba_cart');
					foreach ($ext_aph_cart as $id=>$val){
							$request_ba[$id]= $val;
					}
					
				}
			
				
				$request_ba[$ba_doc][] = $doc_id;
				
				$this->session->set_userdata('ext_ba_cart',$request_ba);

				

				$doc_add = 1;
				$request_ext_aph[$doc_id] =$doc_add;

				$this->session->set_userdata('ext_aph_cart',$request_ext_aph);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				
				$arr = array();
				
				$arr['update_cart'] = array_sum($update_cart);
				echo json_encode($arr);
			}
			else{
				redirect('');
			}
		}
			
		function add_request_id($request_id){
			/*insert request_id into session*/
			$request_item = array();

			if ($this->session->userdata('request_item')){
				$request = $this->session->userdata('request_item');
				foreach ($request as $id=>$val){
					$request_item[$id] = $val;
				}
			}
			
			$request_add = 1;
			$request_item[$request_id] = $request_add;


			$this->session->set_userdata('request_item',$request_item);

			$this->session->set_flashdata('msg', $request_id);
			redirect('transaction/checkout');
		}


		function checkout(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				
				$data['title'] = "Checkout Page";
				/*insert to auth_lib as session_id*/
				$data['profile'] 		= $this->auth_lib->profile($session);
				$data['avatar']			= $this->auth_lib->avatar($session);
				$data['document']		= $this->auth_lib->get_document($session);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				$data['update_cart'] = $update_cart;

				$data['content'] 		= $this->load->view('checkout', $data, TRUE);
				$data['content_css'] 	= '';
				$data['content_js'] 	= '';
				$this->load->view('frame-template',$data);
				
				
			}
			else{
				redirect('');
			}
		}

		function empty_cart() {
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$this->session->unset_userdata('int_aph_cart');
				$this->session->unset_userdata('corporate_cart');
				$this->session->unset_userdata('ext_aph_cart');
				$this->session->unset_userdata('ext_ba_cart');
				$this->session->unset_userdata('cor_to_aph_cart');

				$arr = array();
				$arr['update_cart'] = 0; 
				echo json_encode($arr);
				exit;
				redirect('transaction/checkout');
			}
			else{
				redirect('');
			}
		}

		function empty_cart_int_aph(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){

				$request_id = $this->uri->segment('3');

				$this->session->unset_userdata('int_aph_cart');
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 

				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
				
				
			}
			else{
				redirect('');
			}
		}

		function empty_cart_ext_aph(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){

				$request_id = $this->uri->segment('3');

				$this->session->unset_userdata('ext_aph_cart');
				$this->session->unset_userdata('ext_ba_cart');
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 

				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
			}
			else{
				redirect('');
			}
		}

		function empty_cart_corporate(){
			$request_id = $this->uri->segment('3');

			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$this->session->unset_userdata('corporate_cart');
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 

				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
			}
			else{
				redirect('');
			}
		}

		function empty_cart_cor_to_aph(){
			$request_id = $this->uri->segment('3');

			$session = $this->session->userdata('session');
			$role = $this->session->userdata('role');

			if ($session != null && $role != null){
				// doc_id in array
				$doc_id = $this->input->post('documentId');

				$ba_cart = $this->session->userdata('ext_ba_cart');
				
				foreach ($doc_id as $data){
					$detail_doc = $this->auth_lib->get_detail_document($data);
					foreach ($detail_doc->documentProfiles as $data){
						$ba_id = $data->businessAssociate;
					}
					
				}
				// remove session based on ba_id
				unset($ba_cart[$ba_id]);
				$this->session->set_userdata('ext_ba_cart',$ba_cart);


				//function to remove cor_to_aph_cart based doc_id array
				$new_doc_id = array();
				foreach ($doc_id as $data){
					$new_doc_id[$data] = 1;
				}
				
				if ($this->session->userdata('cor_to_aph_cart')){
					$cor_to_aph = $this->session->userdata('cor_to_aph_cart');
					foreach($new_doc_id as $key => $val){			
						unset($cor_to_aph[$key]);
					}
				}
				$this->session->set_userdata('cor_to_aph_cart', $cor_to_aph);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				
				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
			}
			else{
				redirect('');
			}
		}

		function empty_cart_cor_to_aph_send(){
			$session = $this->session->userdata('session');
			$role = $this->session->userdata('role');

			if ($session != null && $role != null){
				$request_id = $this->uri->segment('4');

				// get ba_id from url
				$ba_id = $this->uri->segment('3');
				
				//get ba_cart session
				$ba_cart = $this->session->userdata('ext_ba_cart');
				
				// save document array based on ba_cart session into variable
				$doc_id = $ba_cart[$ba_id];
			
				//function to remove ext_aph_cart based doc_id array
				$new_doc_id = array();
				foreach ($doc_id as $data){
					$new_doc_id[$data] = 1;
				}


				if ($this->session->userdata('cor_to_aph_cart')){
					$cor_aph_cart = $this->session->userdata('cor_to_aph_cart');
					foreach($new_doc_id as $key => $val){			
						unset($cor_aph_cart[$key]);
					}
				}
				$this->session->set_userdata('cor_to_aph_cart', $cor_aph_cart);

				// remove session based on ba_id
				unset($ba_cart[$ba_id]);
				$this->session->set_userdata('ext_ba_cart',$ba_cart);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 

				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
			}
			else{
				redirect('');
			}
		}

		function empty_cart_ba_send(){
			$session = $this->session->userdata('session');
			$role = $this->session->userdata('role');

			if ($session != null && $role != null){
				$request_id = $this->uri->segment('4');

				// get ba_id from url
				$ba_id = $this->uri->segment('3');

				//get ba_cart session
				$ba_cart = $this->session->userdata('ext_ba_cart');
				
				// save document array based on ba_cart session into variable
				$doc_id = $ba_cart[$ba_id];
			
				//function to remove ext_aph_cart based doc_id array
				$new_doc_id = array();
				foreach ($doc_id as $data){
					$new_doc_id[$data] = 1;
				}

				
				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach($new_doc_id as $key => $val){			
						unset($ext_aph_cart[$key]);
					}
				}
				$this->session->set_userdata('ext_aph_cart', $ext_aph_cart);
				
				// remove session based on ba_id
				unset($ba_cart[$ba_id]);
				$this->session->set_userdata('ext_ba_cart',$ba_cart);


				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 

				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}

			}
			else{
				redirect('');
			}
			
		}

		function empty_cart_ba(){
			$request_id = $this->uri->segment('3');

			$session = $this->session->userdata('session');
			$role	= $this->session->userdata('role');

			if($session != null && $role != null){
				// doc_id in array
				$doc_id = $this->input->post('documentId');

				$ba_cart = $this->session->userdata('ext_ba_cart');
				
				// remove ba_cart session
				foreach ($doc_id as $data){
					$detail_doc = $this->auth_lib->get_detail_document($data);
					foreach ($detail_doc->documentProfiles as $data){
						$ba_id = $data->businessAssociate;
					}
					
				}
								
				// remove session based on ba_id
				unset($ba_cart[$ba_id]);
				$this->session->set_userdata('ext_ba_cart',$ba_cart);
				

				//function to remove ext_aph_cart based doc_id array
				$new_doc_id = array();
				foreach ($doc_id as $data){
					$new_doc_id[$data] = 1;
				}

				
				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach($new_doc_id as $key => $val){			
						unset($ext_aph_cart[$key]);
					}
				}
				$this->session->set_userdata('ext_aph_cart', $ext_aph_cart);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				
				/*insert request_id into session*/
				if ($request_id){
					$this->add_request_id($request_id);
				}
				else{
					redirect('transaction/checkout');
				}
			}
			else{
				redirect('');
			}
		}

		function empty_external(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				$doc_id = $this->input->post('documentId');
				/*function to remove selected item*/

				$new_doc_id = array();
				foreach ($doc_id as $data){
					$new_doc_id[$data] = 1;
				}

				
				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach($new_doc_id as $key => $val){
						
						unset($ext_aph_cart[$key]);
					}
				}
				$this->session->set_userdata('ext_aph_cart', $ext_aph_cart);
				var_dump($this->session->userdata('ext_aph_cart'));
				
				exit;

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				redirect('transaction/checkout');
			}
			else{
				redirect('');
			}
		}

		function remove(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*function to remove selected item*/
				$request_document = array();
				if ($this->session->userdata('int_aph_cart')){
					$int_aph_cart = $this->session->userdata('int_aph_cart');
					foreach($int_aph_cart as $id=>$val){
						if ($this->input->post('documentId') == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('int_aph_cart', $request_document);
				$int_aph_cart = $this->session->userdata('int_aph_cart');
				$arr = array();
				$arr['update_cart'] = array_sum($int_aph_cart);
				echo json_encode($arr);
			}
			else{
				redirect('');
			}	
		}

		function remove_int_aph(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*function to remove selected item*/
				$request_document = array();
				if ($this->session->userdata('int_aph_cart')){
					$int_aph_cart = $this->session->userdata('int_aph_cart');
					foreach($int_aph_cart as $id=>$val){
						if ($this->input->post('documentId') == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('int_aph_cart', $request_document);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				echo json_encode($arr);
			}
			else{
				redirect('');
			}	
		}

		function remove_cor_to_aph(){
			$session = $this->session->userdata('session');
			$role 	= $this->session->userdata('role');

			if($session != null && $role != null){
				// get doc_id and ba_id from document
				$doc_id = $this->input->post('documentId');
				
				$detail_doc = $this->auth_lib->get_detail_document($doc_id);
				foreach ($detail_doc->documentProfiles as $data){
					$ba_id = $data->businessAssociate;
				}
				
				$ba_cart = $this->session->userdata('ext_ba_cart');

				// remove doc_id from ba_cart session
				foreach ($ba_cart as $key => $value){
					$key= array_search($doc_id, $value);
				}
				unset($ba_cart[$ba_id][$key]);
				// set ba_cart session again
				$this->session->set_userdata('ext_ba_cart',$ba_cart);

				// function to remove selected item from cor_to_aph_cart session
				$request_document = array();
				if ($this->session->userdata('cor_to_aph_cart')){
					$cor_to_aph_cart = $this->session->userdata('cor_to_aph_cart');
					foreach($cor_to_aph_cart as $id=>$val){
						if ($doc_id == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('cor_to_aph_cart', $request_document);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				echo json_encode($arr);

			}
			else{
				redirect('');
			}
		}

		function remove_ext_ba_aph(){
			$session = $this->session->userdata('session');
			$role	=  $this->session->userdata('role');

			if ($session != null && $role != null){
				// get doc_id and ba_id from document
				$doc_id = $this->input->post('documentId');
				
				$detail_doc = $this->auth_lib->get_detail_document($doc_id);
				foreach ($detail_doc->documentProfiles as $data){
					$ba_id = $data->businessAssociate;
				}
				
				$ba_cart = $this->session->userdata('ext_ba_cart');
				
				// remove doc_id from ba_cart session
				foreach ($ba_cart as $key => $value){
					$key= array_search($doc_id, $value);
				}
				unset($ba_cart[$ba_id][$key]);
				// set ba_cart session again
				$this->session->set_userdata('ext_ba_cart',$ba_cart);

				// function to remove selected item from ext_aph_cart session
				$request_document = array();
				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach($ext_aph_cart as $id=>$val){
						if ($doc_id == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('ext_aph_cart', $request_document);

				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				echo json_encode($arr);
			}
			else{
				redirect('');
			}
		}

		function remove_ext_aph(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*function to remove selected item*/
				$request_document = array();
				if ($this->session->userdata('ext_aph_cart')){
					$ext_aph_cart = $this->session->userdata('ext_aph_cart');
					foreach($ext_aph_cart as $id=>$val){
						if ($this->input->post('documentId') == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('ext_aph_cart', $request_document);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				echo json_encode($arr);
			}
			else{
				redirect('');
			}	
		}

		function remove_corporate(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*function to remove selected item*/
				$request_document = array();
				if ($this->session->userdata('corporate_cart')){
					$corporate_cart = $this->session->userdata('corporate_cart');
					foreach($corporate_cart as $id=>$val){
						if ($this->input->post('documentId') == $id){

						} else {
							$request_document[$id] = $val;
						}
					}
				}
				$this->session->set_userdata('corporate_cart', $request_document);
				
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
				

				$arr = array();
				$arr['update_cart'] =  array_sum($update_cart); 
				echo json_encode($arr);
			}
			else{
				redirect('');
			}
		}


		function list_cart(){
			$session=$this->session->userdata('session');
			$role = $this->session->userdata('role');
			
			if ($session != null && $role != null){
				/*get total cart session*/
				$total_cart = array();
				$total_cart[]	= $this->session->userdata('int_aph_cart');
				$total_cart[]	= $this->session->userdata('corporate_cart');
				$total_cart[] 	= $this->session->userdata('ext_aph_cart');
				$total_cart[] 	= $this->session->userdata('cor_to_aph_cart');

				$update_cart = array();
				foreach($total_cart as $arrs) {
				    if(is_array($arrs)) {
				        $update_cart = array_merge($update_cart, $arrs);
				    }
				}
			
				$list_cart = array();

				if ($update_cart == null){
					$list_cart = array();
					
				}
				else{
					foreach ($update_cart as $key => $data){
						array_push($list_cart, $key);
					}
					echo json_encode($list_cart);
				}
				
			}
			else{
				redirect('');
			}
			
		}

	}
?>