<div class="panel row">
	
	<?php 
			/*digunakan untuk menampung data session request cart yang sudah dipilih*/
			$request_cart = @$this->session->userdata('request_cart');
			/*var_dump($request_cart);*/
		?>
	<div class="col-md-12">
		<!-- <a href="<?php echo site_url('/transaction/checkout') ?>" class="btn btn-primary">
		 	Checkout (<span id="update_cart"><?php if($request_cart){echo array_sum($request_cart);} else { echo '0'; } ?></span>)
		</a> -->
	</div>

	<div class="col-md-12">
	<div class="col-md-9">
	
	<h3>Request Page</h3>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
		          <th>Document ID</th>
		          <th>
		            Document Name
		          </th>
		          <th>
		           	Digital
		          </th>
		          <th>
		          	Physical
		          </th>
		          <th>
		            Action
		          </th>
		        </tr>
			</thead>
			<?php foreach ($document->documents as $data){ ?>
			
			<tbody>
				<tr>
					<td> <?php echo $data->documentId; ?> </td>
					<td> <?php echo $data->documentName; ?></td>
					<td> <?php if ($data->isDigital == 1 ) { echo "Yes";} else {echo "No";} ?> </td>
					<td> <?php if ($data->isPhysical == 1) { echo "Yes";} else {echo "No";} ?></td>
					<?php 
						echo form_open('transaction/add'); 
						echo form_hidden('documentId', $data->documentId);
						
					?>
					<td>
						<button type="submit" name="action" class="btn btn-primary">
					 		<span class="glyphicon glyphicon-shopping-cart"> </span>
					 		Add to chart
						 </button>
					<?php echo form_close(); ?>
					</td>
				</tr>
			</tbody>
			
			<?php } ?>
		</table>
		
	</div>

	<div class="col-md-3">
	</div>

	</div>
</div>
