<script src="<?php echo site_url('assets/js/jquery.min.js')?>"></script>

<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Checkout</strong>Page</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Checkout</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->


<?php $flash= $this->session->flashdata('msg'); 
        if(!empty($flash)): ?>
    <div class="row">
    	<div class="col-md-9">
	    	<div class="alert alert-success media fade in ">
		        <button type="button" class="close" data-dismiss="alert">&times; &nbsp;</button>
		        &nbsp;  Your request has been sent. Click <a href="<?php echo site_url('profile/request_detail/'.$flash) ?>"> here </a> to preview.
		    </div> 
	    </div> 

	    <div class="col-md-3">
	    </div>
    </div>
    
    
    <?php endif; ?>

<?php 
	//var_dump($this->session->userdata('request_item'));
	if ($update_cart){
?>
	
	<?php
		$int_aph 	= $this->session->userdata('int_aph_cart');
		$ext_aph    = $this->session->userdata('ext_aph_cart');
		$cor_aph 	= $this->session->userdata('corporate_cart');
		$cor_to_aph = $this->session->userdata('cor_to_aph_cart');

	/*	var_dump($ext_aph);
		echo "<br/>";
	
		echo "<br/>";
		$array = array('foo' => '2', 'hello' => '2');
		
		end($ext_aph);
		$key = key($ext_aph);
		var_dump($key);*/
		

		if ($int_aph){

	?>
	<div class="row">
		<div class="col-xlg-12">
			<div class="panel">
		   		<div class="panel-header panel-controls" style="cursor:pointer" data-toggle="collapse" data-target="#collapseintaph" aria-expanded="false" aria-controls="collapseintaph">
		      		<h3><i class="icon-basket"></i> <strong>Internal APH</strong> Request</h3>
		    	</div>
			    <div class="panel-content widget-full widget-stock stock1">
			    	<div class="col-md-10">
			    		<div class="collapse" id="collapseintaph">
			     		<div id="notification_int"> </div>
				     	<table class="table  table-hover" id="checkout_table" >
				     		<thead>
								<tr>
								  <th> Date </th>
						          <th>Document ID</th>
						          <th>
						            Document Name
						          </th>
						          <th>
						           	Digital
						          </th>
						          <th>
						          	Physical
						          </th>
						          <th>
						            Action
						          </th>
						        </tr>
							</thead>

							<tbody id="mytbody1">
								<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
								<?php 
									foreach ($int_aph as $rc=>$value){
										/*$rc adalah documentId*/
										$detail = $this->auth_lib->get_detail_document($rc);
							
										foreach ($detail->documentProfiles as $data){
								?>
								<tr id="tr<?php echo $rc;?>">
									<input type="hidden" name="documentId" value="<?php echo $data->documentId ?>"  id="documentId">
									<td>
									 	<?php
									 		$date = date("Y-m-d");
									 		$arr_date = explode("-", $date);
                                            $date= $arr_date[2];
                                            $month = $arr_date[1];
                                            $year = $arr_date[0];
                                            switch ($month){
                                                case 1 : $month ='Jan'; break;
                                                case 2 : $month ='Feb'; break;
                                                case 3 : $month = 'Mar'; break;
                                                case 4 : $month = 'Apr'; break;
                                                case 5 : $month = 'May'; break;
                                                case 6 : $month = 'Jun'; break;
                                                case 7 : $month = 'Jul'; break;
                                                case 8 : $month = 'Aug'; break;
                                                case 9 : $month = 'Sept'; break;
                                                case 10 : $month = 'Oct'; break;
                                                case 11 : $month = 'Nov'; break;
                                                case 12 : $month = 'Dec'; break;
                                            }
                                            echo $date.' '.$month.' '.$year;
									 	 ?>
									 </td>
									<td> <?php echo $data->documentId; ?> </td> 
									<td> <?php echo $data->documentName; ?></td>
									<td> <?php if ($data->isDigital == 1 ) { echo "Yes";} else {echo "No";} ?> </td>
									<td> <?php if ($data->isPhysical == 1) { echo "Yes";} else {echo "No";} ?></td>		
									<td>
										<a  class="remove_int_aph btn btn-danger btn-xs" totalid="<?php echo array_sum($update_cart); ?>" documentId="<?php echo $rc ?>"> 
									 		<span class="glyphicon glyphicon-remove"> </span>
									 		Remove
										 </a>
									</td>
								</tr>

								<?php
									}
								 } ?>
							</tbody>
				     	</table>
				     		<!-- button -->
						<?php $url = site_url('/transaction/empty_cart_int_aph');
				                    $a = ltrim($url,"http://");
				               ?>
						<form action="<?php echo $this->auth_lib->url().'/submit/request/'.$a; ?>"  method="post">
					
							<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
							
								<?php 
									foreach ($int_aph as $rc=>$value){
										/*$rc adalah documentId*/
										$detail = $this->auth_lib->get_detail_document($rc);
										foreach ($detail->documentProfiles as $data){
											$doc = $data->documentId;
											echo form_hidden('document', $doc);

								?>
								
								<?php }
								 } 	?> 
							<div id="button_bottom">	 
								<button type="submit" class="btn btn-primary" id="submitbutton">
							 		<span class="glyphicon glyphicon-send"> </span>
							 		 Send
								</button>
						</form>
								&nbsp;
								<a href="<?php echo site_url('transaction/empty_cart_int_aph'); ?>" class="btn btn-danger">
									<span class="glyphicon glyphicon-trash"> </span>
							 		 Empty cart
								</a>
							</div>
							<br/>
					</div>
			    	</div>
			     	
			    </div>
			     	
			</div>
		</div>
	</div>
	
	<?php } ?>

	<?php if ($cor_to_aph) { ?>
	<div class="row">
		<div class="col-xlg-12">
			<div class="panel">
				<div class="panel-header panel-controls" style="cursor:pointer" data-toggle="collapse" data-target="#collapsecoraph" aria-expanded="false" aria-controls="collapseextaph">
		      		<h3><i class="icon-basket"></i> <strong>APH</strong> Request</h3>
		    	</div>

		    	<div class="panel-content widget-full widget-stock stock1">
		    		<div class="col-md-10">
		    			<div class="collapse" id="collapsecoraph">
		    				<?php 

		    					$ba_cart = $this->session->userdata('ext_ba_cart');
		    					foreach ($ba_cart as $key => $value){
		    						$baName = $this->auth_lib->business_associate($key);
		    						foreach ($baName->avatarBA as $ba){
		    				 ?>
		    				<h5 class="text-info"><strong><?php echo $ba->businessAssociate->baName;  }?> </strong></h5>
		    				<div id="notification_cor_to_aph"> </div>
		    				<table class="table  table-hover" id="checkout_table">
								<thead>
									<tr>
									<th> Date </th>
							          <th>Document ID</th>
							          <th>
							            Document Name
							          </th>
							          <th>
							           	Digital
							          </th>
							          <th>
							          	Physical
							          </th>
							          <th>
							            Action
							          </th>
							        </tr>
								</thead>

								<tbody id="mytbody2">
								<?php 
									foreach ($value as $keys=> $values){ 
										$detail = $this->auth_lib->get_detail_document($values);
										foreach ($detail->documentProfiles as $data){ 
								?>
									<tr id="tr<?php echo $data->documentId;?>">
										<input type="hidden" name="documentId" value="<?php echo $data->documentId ?>"  id="documentId">
										<td>
										 	<?php
										 		$date = date("Y-m-d");
										 		$arr_date = explode("-", $date);
	                                            $date= $arr_date[2];
	                                            $month = $arr_date[1];
	                                            $year = $arr_date[0];
	                                            switch ($month){
	                                                case 1 : $month ='Jan'; break;
	                                                case 2 : $month ='Feb'; break;
	                                                case 3 : $month = 'Mar'; break;
	                                                case 4 : $month = 'Apr'; break;
	                                                case 5 : $month = 'May'; break;
	                                                case 6 : $month = 'Jun'; break;
	                                                case 7 : $month = 'Jul'; break;
	                                                case 8 : $month = 'Aug'; break;
	                                                case 9 : $month = 'Sept'; break;
	                                                case 10 : $month = 'Oct'; break;
	                                                case 11 : $month = 'Nov'; break;
	                                                case 12 : $month = 'Dec'; break;
	                                            }
	                                            echo $date.' '.$month.' '.$year;
										 	 ?>
										 </td>
										<td> <?php echo $data->documentId; ?> </td> 
										<td> <?php echo $data->documentName; ?></td>
										<td> <?php if ($data->isDigital == 1 ) { echo "Yes";} else {echo "No";} ?> </td>
										<td> <?php if ($data->isPhysical == 1) { echo "Yes";} else {echo "No";} ?></td>		
										<td>									
											<a   class="remove_cor_to_aph btn btn-danger btn-xs" totalid="<?php echo array_sum($update_cart); ?>"  documentId="<?php echo $values ?>"> 
										 		<span class="glyphicon glyphicon-remove"> </span>
										 		Remove
											 </a>
										
										</td>
									</tr>
									<?php			
										}
									}
									?>
								</tbody>
							</table>

							<table id="button_bottom" >
								<tr> 
									<td>
										<?php
											 $url = site_url('/transaction/empty_cart_cor_to_aph_send/'.$key);
								                    $a = ltrim($url,"http://");
								        ?>
							        	<form action="<?php echo $this->auth_lib->url().'/submit/request/'.$a; ?>"  method="post">
											<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
											<?php 
												foreach ($value as $key=> $values){
													/*$rc adalah documentId*/
													$detail = $this->auth_lib->get_detail_document($values);
													foreach ($detail->documentProfiles as $data){
														$doc = $data->documentId;
														echo form_hidden('document', $doc);
													 }
												 } 	?> 
												 
											<button type="submit" class="btn btn-primary" id="submitbutton">
										 		<span class="glyphicon glyphicon-send"> </span>
										 		 Send
											</button>
										</form>
									</td>
									<td>
											<form action="<?php echo site_url('transaction/empty_cart_cor_to_aph'); ?>" method="post">
											<?php
												
												foreach ($value as $key=> $values){
													/*$rc adalah documentId*/
													$detail = $this->auth_lib->get_detail_document($values);
													foreach ($detail->documentProfiles as $data){
														$doc = $data->documentId;
														echo form_hidden('documentId[]', $doc);
													 }
												 } 

											?>
												<button type="submit" class="btn btn-danger">
													<span class="glyphicon glyphicon-trash"> </span>
											 		 Empty cart
												</button>
											
											</form>
									</td>
								</tr>
							</table>
							<hr/>

		    				 <?php 
		    				 	// end of foreach ba_cart session
		    				 	}
		    				 ?>
		    			</div>
		    		</div>

		    		<div class="col-md-3">
			    	</div>
		    	</div>
			</div>
		</div>
	</div>

	<?php } ?>

	<?php if ($ext_aph) { ?>
	<div class="row">
		<div class="col-xlg-12">
			<div class="panel">
		   		<div class="panel-header panel-controls" style="cursor:pointer" data-toggle="collapse" data-target="#collapseextaph" aria-expanded="false" aria-controls="collapseextaph">
		      		<h3><i class="icon-basket"></i> <strong>External APH</strong> Request</h3>
		    	</div>
			    <div class="panel-content widget-full widget-stock stock1">
			    	<div class="col-md-10">
			    		<div class="collapse" id="collapseextaph">
				     	
				     		<!-- pembagian tabel berdasarakan peminjaman di tiap business associate -->
				     		<?php 
				     		
									$ba_cart = $this->session->userdata('ext_ba_cart');
									foreach ($ba_cart as $key => $value){

										$baName = $this->auth_lib->business_associate($key);
										foreach ($baName->avatarBA as $ba){
							?>	
							<h5 class="text-info"><strong><?php echo $ba->businessAssociate->baName;  }?> </strong></h5>
							<div id="notification_ext"> </div>
							<table class="table  table-hover" id="checkout_table">
								<thead>
									<tr>
									  <th> Date </th>
							          <th>Document ID</th>
							          <th>
							            Document Name
							          </th>
							          <th>
							           	Digital
							          </th>
							          <th>
							          	Physical
							          </th>
							          <th>
							            Action
							          </th>
							        </tr>
								</thead>

								<tbody id="mytbody3">
								<?php 
									
									foreach ($value as $keys=> $values){ 
										$detail = $this->auth_lib->get_detail_document($values);
										foreach ($detail->documentProfiles as $data){ 
								?>
									<tr id="tr<?php echo $data->documentId;?>">
										<input type="hidden" name="documentId" value="<?php echo $data->documentId ?>"  id="documentId">
										<td>
										 	<?php
										 		$date = date("Y-m-d");
										 		$arr_date = explode("-", $date);
	                                            $date= $arr_date[2];
	                                            $month = $arr_date[1];
	                                            $year = $arr_date[0];
	                                            switch ($month){
	                                                case 1 : $month ='Jan'; break;
	                                                case 2 : $month ='Feb'; break;
	                                                case 3 : $month = 'Mar'; break;
	                                                case 4 : $month = 'Apr'; break;
	                                                case 5 : $month = 'May'; break;
	                                                case 6 : $month = 'Jun'; break;
	                                                case 7 : $month = 'Jul'; break;
	                                                case 8 : $month = 'Aug'; break;
	                                                case 9 : $month = 'Sept'; break;
	                                                case 10 : $month = 'Oct'; break;
	                                                case 11 : $month = 'Nov'; break;
	                                                case 12 : $month = 'Dec'; break;
	                                            }
	                                            echo $date.' '.$month.' '.$year;
										 	 ?>
										 </td>
										<td> <?php echo $data->documentId; ?> </td> 
										<td> <?php echo $data->documentName; ?></td>
										<td> <?php if ($data->isDigital == 1 ) { echo "Yes";} else {echo "No";} ?> </td>
										<td> <?php if ($data->isPhysical == 1) { echo "Yes";} else {echo "No";} ?></td>		
										<td>									
											<a   class="remove_ext_ba_aph btn btn-danger btn-xs" totalid="<?php echo array_sum($update_cart); ?>"  documentId="<?php echo $values ?>"> 
										 		<span class="glyphicon glyphicon-remove"> </span>
										 		Remove
											 </a>
										
										</td>
									</tr>
									<?php			
										}
									}
									?>
								</tbody>
							</table>
						
							<table id="button_bottom" >
								<tr> 
									<td>
										<?php
											$url = site_url('/transaction/empty_cart_ba_send/'.$key);
									                    $a = ltrim($url,"http://");
								            
								        ?>
							        	<form action="<?php echo $this->auth_lib->url().'/submit/request/'.$a; ?>"  method="post">
											<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
											<?php 
												foreach ($value as $key=> $values){
													/*$rc adalah documentId*/
													$detail = $this->auth_lib->get_detail_document($values);
													foreach ($detail->documentProfiles as $data){
														$doc = $data->documentId;
														echo form_hidden('document', $doc);
													 }
												 } 	?> 
												 
											<button type="submit" class="btn btn-primary" id="submitbutton">
										 		<span class="glyphicon glyphicon-send"> </span>
										 		 Send
											</button>
										</form>
									</td>
									<td>
											<form action="<?php echo site_url('transaction/empty_cart_ba'); ?>" method="post">
											<?php
												
												foreach ($value as $key=> $values){
													/*$rc adalah documentId*/
													$detail = $this->auth_lib->get_detail_document($values);
													foreach ($detail->documentProfiles as $data){
														$doc = $data->documentId;
														echo form_hidden('documentId[]', $doc);
													 }
												 } 

											?>
												<button type="submit" class="btn btn-danger">
													<span class="glyphicon glyphicon-trash"> </span>
											 		 Empty cart
												</button>
											
											</form>
									</td>
								</tr>
							</table>
							<hr/>

							<?php
								// end of foreach ba_cart session
									}
								?>
						</div>
			    	</div>

			    	<div class="col-md-3">
			    	</div>
			    </div>
			</div>
		</div>
	</div>
	
	<?php } ?>


	<?php if ($cor_aph){ ?>
	<div class="row">
		<div class="col-xlg-12">
			<div class="panel">
		   		<div class="panel-header panel-controls" style="cursor:pointer" data-toggle="collapse" data-target="#collapsecor" aria-expanded="false" aria-controls="collapsecor">
		      		<h3><i class="icon-basket"></i> <strong>Corporate</strong> Request</h3>
		    	</div>
			    <div class="panel-content widget-full widget-stock stock1">
			    	<div class="col-md-10">
			    		<div class="collapse" id="collapsecor">
			     		<div id="notification_cor"> </div>
				     	<table class="table  table-hover" id="checkout_table" >
				     		<thead>
								<tr>
								  <th> Date </th>
						          <th>Document ID</th>
						          <th>
						            Document Name
						          </th>
						          <th>
						           	Digital
						          </th>
						          <th>
						          	Physical
						          </th>
						          <th>
						            Action
						          </th>
						        </tr>
							</thead>

							<tbody id="mytbody4">
								<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
								<?php 
									foreach ($cor_aph as $rc=>$value){
										/*$rc adalah documentId*/
										$detail = $this->auth_lib->get_detail_document($rc);
							
										foreach ($detail->documentProfiles as $data){
								?>
								<tr id="tr<?php echo $rc;?>">
									<input type="hidden" name="documentId" value="<?php echo $data->documentId ?>"  id="documentId">
									<td>
									 	<?php
									 		$date = date("Y-m-d");
									 		$arr_date = explode("-", $date);
                                            $date= $arr_date[2];
                                            $month = $arr_date[1];
                                            $year = $arr_date[0];
                                            switch ($month){
                                                case 1 : $month ='Jan'; break;
                                                case 2 : $month ='Feb'; break;
                                                case 3 : $month = 'Mar'; break;
                                                case 4 : $month = 'Apr'; break;
                                                case 5 : $month = 'May'; break;
                                                case 6 : $month = 'Jun'; break;
                                                case 7 : $month = 'Jul'; break;
                                                case 8 : $month = 'Aug'; break;
                                                case 9 : $month = 'Sept'; break;
                                                case 10 : $month = 'Oct'; break;
                                                case 11 : $month = 'Nov'; break;
                                                case 12 : $month = 'Dec'; break;
                                            }
                                            echo $date.' '.$month.' '.$year;
									 	 ?>
									 </td>
									<td> <?php echo $data->documentId; ?> </td> 
									<td> <?php echo $data->documentName; ?></td>
									<td> <?php if ($data->isDigital == 1 ) { echo "Yes";} else {echo "No";} ?> </td>
									<td> <?php if ($data->isPhysical == 1) { echo "Yes";} else {echo "No";} ?></td>		
									<td>
										<a  class="remove_corporate btn btn-danger btn-xs" totalid="<?php echo array_sum($update_cart); ?>" documentId="<?php echo $rc ?>"> 
									 		<span class="glyphicon glyphicon-remove"> </span>
									 		Remove
										 </a>
									</td>
								</tr>

								<?php
									}
								 } ?>
							</tbody>
				     	</table>
				     		<!-- button -->
						<?php $url = site_url('/transaction/empty_cart_corporate');
				                    $a = ltrim($url,"http://");
				               ?>
						<form action="<?php echo $this->auth_lib->url().'/submit/request/'.$a; ?>"  method="post">
					
							<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>"> 
							
								<?php 
									foreach ($cor_aph as $rc=>$value){
										/*$rc adalah documentId*/
										$detail = $this->auth_lib->get_detail_document($rc);
										foreach ($detail->documentProfiles as $data){
											$doc = $data->documentId;
											echo form_hidden('document', $doc);

								?>
								
								<?php }
								 } 	?> 
							<div id="button_bottom">	 
								<button type="submit" class="btn btn-primary" id="submitbutton">
							 		<span class="glyphicon glyphicon-send"> </span>
							 		 Send
								</button>
						</form>
								&nbsp;
								<a href="<?php echo site_url('transaction/empty_cart_corporate'); ?>" class="btn btn-danger">
									<span class="glyphicon glyphicon-trash"> </span>
							 		 Empty cart
								</a>
							</div>	
							<br/>
						</div>
			    	</div>
			      	
			    </div>
			</div>
		</div>	
	</div>

	<?php } ?>
	
<?php		
	}
	else{

?>
	<div class="panel row" style="min-height:480px">
		<div class="col-md-12">
			<div class="col-md-9">
				<h3><strong>Detail</strong> Request</h3>
				<div class="alert media fade in alert-warning ">
					Your <strong>request</strong> is now empty. 
				</div>
			</div>			
		</div>		
	</div>

<?php } ?>

<SCRIPT TYPE="text/javascript">
	$(document).ready(function(){
		

		$(".remove_int_aph").click(function(){	
			var x = confirm("Delete item ?");
			var documentId = $(this).attr('documentId');
			var total = $(this).attr('totalid');
			if (x == true){
				$("#notification_int").html('<div class="alert alert-info" role="alert">Please wait ...</div>');
				var dataString  = { documentId  : documentId };
				console.log(dataString);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>transaction/remove_int_aph",
					data: dataString,
					dataType: "json",
					cache		: false,
					success: function(data){
						$("#tr"+documentId).remove();
						$("#notification_int").html('<div class="alert alert-success" role="alert">Success delete item ...</div>');
						var count = $('#mytbody1 > TR').length;
						// console.log(count);
						if(count == 0){
							$('#button_bottom').empty();
							$('#checkout_table').empty();
							$("#notification_int").html('<div class="alert alert-warning" role="alert">Your <strong>request</strong> is now empty</div>');
						}
						$("#update_cart").html(data.update_cart);	
					}, error: function(xhr, status, error) {
							alert(status);
						},
				});
			}
			else {
				return false;
			}
		});
	

		$(".remove_ext_ba_aph").click(function(){	
			var x = confirm("Delete item ?");
			var documentId = $(this).attr('documentId');
			var total = $(this).attr('totalid');
			if (x == true){
				$("#notification_ext").html('<div class="alert alert-info" role="alert">Please wait ...</div>');
				var dataString  = { documentId  : documentId};
				console.log(dataString);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>transaction/remove_ext_ba_aph",
					data: dataString,
					dataType: "json",
					cache		: false,
					success: function(data){
						$("#tr"+documentId).remove();
						$("#notification_ext").html('<div class=" alert alert-success " role="alert">Success delete item ...</div>');
						var count = $('#mytbody3 > TR').length;
						// console.log(count);
						if(count == 0){
							$('#button_bottom').empty();
							$('#checkout_table').empty();
							$("#notification_ext").html('<div class="alert alert-warning" role="alert">Your <strong>request</strong> is now empty</div>');
						}
						$("#update_cart").html(data.update_cart);	
					}, error: function(xhr, status, error) {
							alert(status);
						},
				});
			}
			else {
				return false;
			}
		});

		$(".remove_cor_to_aph").click(function(){
			var x = confirm("Delete item ?");
			var documentId = $(this).attr('documentId');
			var total = $(this).attr('totalid');
			if (x == true){
				$("#notification_cor_to_aph").html('<div class="alert alert-info" role="alert">Please wait ...</div>');
				var dataString  = { documentId  : documentId};
				console.log(dataString);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>transaction/remove_cor_to_aph",
					data: dataString,
					dataType: "json",
					cache		: false,
					success: function(data){
						$("#tr"+documentId).remove();
						$("#notification_cor_to_aph").html('<div class=" alert alert-success " role="alert">Success delete item ...</div>');
						var count = $('#mytbody2 > TR').length;
						// console.log(count);
						if(count == 0){
							$('#button_bottom').empty();
							$('#checkout_table').empty();
							$("#notification_cor_to_aph").html('<div class="alert alert-warning" role="alert">Your <strong>request</strong> is now empty</div>');
						}
						$("#update_cart").html(data.update_cart);	
					}, error: function(xhr, status, error) {
							alert(status);
						},
				});
			}
			else {
				return false;
			}
		});

		$(".remove_corporate").click(function(){	
			var x = confirm("Delete item ?");
			var documentId = $(this).attr('documentId');
			var total = $(this).attr('totalid');
			if (x == true){
				$("#notification_cor").html('<div class="alert alert-info" role="alert">Please wait ...</div>');
				var dataString  = { documentId  : documentId };
				console.log(dataString);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>transaction/remove_corporate",
					data: dataString,
					dataType: "json",
					cache		: false,
					success: function(data){
						$("#tr"+documentId).remove();
						$("#notification_cor").html('<div class="alert alert-success" role="alert">Success delete item ...</div>');
						var count = $('#mytbody4 > TR').length;
						// console.log(count);
						if(count == 0){
							$('#button_bottom').empty();
							$('#checkout_table').empty();
							$("#notification_cor").html('<div class="alert alert-warning" role="alert">Your <strong>request</strong> is now empty</div>');
						}
						$("#update_cart").html(data.update_cart);	
					}, error: function(xhr, status, error) {
							alert(status);
						},
				});
			}
			else {
				return false;
			}
		});
	});
</SCRIPT>
