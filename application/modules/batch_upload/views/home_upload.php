<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Upload</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">File Upload</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-upload">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>File</strong> Upload</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleUpload"><strong>Document </strong> Upload</h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#upload-tab1-document" data-toggle="tab"><i class="icon-doc"></i> Add Document</a></li>
          <li><a href="#upload-tab2-document" data-toggle="tab"><i class="icon-eye"></i>Document You Uploaded</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="upload-tab1-document">
            <?php $url = site_url('/batch_upload');
                $a = ltrim($url,"http://");
            ?>
            <form action="<?php echo $this->auth_lib->url(); ?>/submit/document/<?php echo $a ?>" method="post" enctype="multipart/form-data" class="forms" id="form-document-upload">
            <form action="http://localhost/POST.PHP" method="post" enctype="multipart/form-data" class="forms" id="form-document-upload">
              <div class="form-group">
                <label for="docId">Document ID</label>
                <input type="text" name="docId" class="form-control">
              </div>
              <div class="form-group">
                <label for="documentName">Document Title</label>
                <input type="text" name="documentName" class="form-control">
              </div>
              <div class="form-group">
                <label for="remark">Remark</label>
                <input type="text" name="remark" class="form-control">
              </div>
              <div class="form-group">
                <label for="docType">Document Type</label>
                <select name="docType" multiple class="form-control" data-placeholder="Document Type (you can choose more than 1)" id="option-upload">
                  <optgroup label="North America">
                              <option value="image-USA">USA</option>
                              <option value="image-CANADA">Canada</option>
                              <option value="image-MEXICO">Mexico</option>
                            </optgroup>
                            <optgroup label="Europe">
                              <option value="image-SPANISH">Spain</option>
                              <option value="image-FRENCH">France</option>
                              <option value="image-UNITED-KINGDOM">United Kingdom</option>
                              <option value="image-italy">Italy</option>
                            </optgroup>
                            <optgroup label="Africa">
                              <option value="image-botswana">Botswana</option>
                              <option value="image-cameroon">Cameroon</option>
                              <option value="image-niger">Niger</option>
                            </optgroup>
                </select>
              </div>
              <hr>
              <select name="isDigital" class="form-control" data-placeholder="Digital?">
                  <option value="0">NO</option>
                  <option value="1">YES</option>
                  <option value="0">NO</option>
                </select>
              <!-- <div class="checkbox">
                <label><input type="checkbox" value="1" name="isDigital"> Digital Document</label>
              </div> -->
                <label for="fileUpload">Upload Document :</label>
                <input type="file" name="fileUpload" id="files-gamabox-document" style="margin: 10px">
                <label for="digitalType">Digital Type :</label>
                <select name="digitalType" class="form-control" data-placeholder="Select Document Type">
                  <option value=""></option>
                  <option value=".pdf">PDF</option>
                  <option value=".docx">DOCX</option>
                  <option value=".xlsx">XLSX</option>
                  <option value=".las">LAS</option>
                  <option value=".segy">SEGY</option>
                </select>
              <hr>
              <select name="isPhysical" class="form-control" data-placeholder="Physical2?">
                  <option value="0">NO</option>
                  <option value="1">YES</option>
                  <option value="0">NO</option>
                </select>
              <!-- <div class="checkbox">
                <label><input type="checkbox" value="1" name="isPhysical"> Physical Document</label>
              </div> -->
                <label for="digitalType">Warehouse :</label>
                <select name="warehouseId" class="form-control" data-placeholder="Select Warehouse">
                  <option value=""></option>
                  <option value="WRH12">Warehouse 12</option>
                  <option value="wrh13">Warehouse 13</option>
                </select>
              <hr>
              <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token'); ?>">
              <button type="submit" class="btn btn-success m-r-20 text-center">Add Document</button>
            </form>
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="upload-tab2-document">
            <div id="uploaded-document">
              
            </div>
          </div>
        </div>
        <!-- END Tab  -->
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-upload">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>File</strong> Upload</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleUpload"><strong>Batch </strong> Upload</h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#upload-tab1" data-toggle="tab"><i class="icon-info"></i> File/Files Upload</a></li>
          <li><a href="#upload-tab2" data-toggle="tab"><i class="icon-eye"></i>Upload Template</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="upload-tab1">
            <!-- NEW UPLOAD -->
            <div class="text-center">
              <p class="text-center">*please use upload template from gamabox-ep system (on the next tab)</p>
              <form action="<?php echo $this->auth_lib->url(); ?>/submit/batch/67" method="post" enctype="multipart/form-data" class="forms" id="form-batch-upload">
                <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token'); ?>">
                <input type="file" name="file_upload" id="files-gamabox">
                <input type="hidden" name="value" value="" id="value-form">
                <input type="hidden" name="fileName" value="" id="fileName-form">
                <br>
                <button type="button" class="btn btn-primary m-r-20 text-center" id="check-parse-gamabox">Check File</button>
                <div id="afterparse-table-gamabox">
                </div>
                <button type="submit" class="btn btn-success m-r-20 text-center" id="submit-form">Upload</button>
              </form>  
            </div>
            
            <hr>
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="upload-tab2">
            <div id="template_table">
              
            </div>
          </div>
        </div>
        <!-- END Tab  -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  
</script>

 