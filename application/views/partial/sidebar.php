<?php 
			/*digunakan untuk menampung data session request cart yang sudah dipilih*/
			$request_cart = @$this->session->userdata('request_cart');
			/*var_dump($request_cart);*/
		?>
<div class="logopanel">
	<h1>
    	<a href="<?php echo site_url('') ?>"></a>
  	</h1>
</div>
<div class="sidebar-inner">
  	<div class="sidebar-top">
	    <form action="search-result.html" method="post" class="searchform" id="search-results">
	      <input type="text" class="form-control" name="keyword" placeholder="Pencarian">
	    </form>
	    <div class="userlogged clearfix">
	      	<!-- show avatar -->
	      	<?php foreach ($avatar->avatarUser as $data){
	      		if ($data->path){ ?>
	      		<img src="<?php foreach($avatar->avatarUser as $data){echo $this->auth_lib->domain().$data->path;}  ?>" class="img-responsive img-circle" alt="friend 8" style="
    				width: 85px;">
	      	<?php	}else{ ?>
	      		<i class="icon icons-faces-users-03"></i>
	      	<?php		}
	      		} ?>
	     
	      	<div class="user-details">
	      		<?php foreach ($profile->user as $data){ ?>
		        <h4><?php echo ucfirst($data->userName);?></h4> 
		        <br/>
		        <?php } ?>
	      	</div>
	    </div>
  	</div>
  	<div class="menu-title">
   		 Menu 
  	</div>
  	<ul class="nav nav-sidebar">
	    <li id="navhome"><a href="<?php echo site_url('') ?>"><i class="icon-home"></i><span data-translate="Home">Home</span></a></li>
	    <li id="navmap"><a href="<?php echo site_url('map')?>"><i class="icon-map"></i><span data-translate="Map">Map</span></a></li>
	    <li id="navtreeview"><a href="<?php echo site_url('tree_view')  ?>"><i class="icon-layers"></i><span data-translate="TreeView">TreeView</span></a></li>
	    <li id="navproduction"><a href="<?php echo site_url('production_report') ?>"><i class="icon-graph"></i><span data-translate="Production Report">Production Report</span></a></li>
	    <li id="navupload"><a href="<?php echo site_url('batch_upload')  ?>"><i class="icon-arrow-up"></i><span data-translate="Batch Upload">Batch Upload</span></a></li>
	    <li id="navdocument"><a href="<?php echo site_url('document')  ?>"><i class="icon-doc"></i><span data-translate="Documents">Documents</span></a></li>
	    <li id="navwarehouse"><a href="<?php echo site_url('warehouse')  ?>"><i class="icon-folder-alt"></i><span data-translate="Warehouse">Warehouse</span></a></li>
	    <li id="navinfo"><a href="<?php echo site_url('dashboard/helpinfo')?>"><i class="icon-info"></i><span data-translate="Info & Help">Info & Help</span></a></li>
  	</ul>
  	<div class="sidebar-footer clearfix">
	    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
	    	<i class="icon-size-fullscreen"></i>
		</a>
		<form  id="myform" action="<?php echo $this->auth_lib->logout(); ?>" method="post" >
        	<input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>">
	    	<a href="javascript: submitform()" class="pull-left btn-effect" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
	    		<i class="icon-power"></i>
	   	 	</a>
	   	</form>
 	</div>
</div>

<script type="text/javascript">
function submitform()
{
    document.forms["myform"].submit();
}
</script>