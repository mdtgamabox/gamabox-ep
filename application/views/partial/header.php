<div class="header-left">
    <div class="topnav">
      <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
    </div>
</div>
<div class="header-right" >
	<ul class="header-menu nav navbar-nav">
		<!-- BEGIN USER DROPDOWN -->
        <?php foreach ($profile->user as $data){                    
                    $ba_id = $this->auth_lib->business_associate($data->businessAssociate);
                    foreach ($ba_id->avatarBA as $ba){                   
                 ?>
		<li class="dropdown" id="user-header">
             <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                <span class="username"> <?php  echo $ba->businessAssociate->baName; ?> &nbsp;</span> 
                 <img src="<?php echo $this->auth_lib->domain().$ba->path; ?>" alt="user image">                
                   <?php } ?>
                    <span class="username"> &nbsp; Hi, <?php echo ucfirst($data->userName);?></span>
                <?php } ?>
            </a>
            <ul class="dropdown-menu">
            	<li>
                    <a href="<?php echo site_url('profile')  ?>"><i class="icon-user"></i><span>My Profile</span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-settings"></i><span>Account Settings</span></a>
                </li>
                <li>
                    <form  id="myform1" action="<?php echo $this->auth_lib->logout(); ?>" method="post" >
                        <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>">
                        <a href="javascript: submitform()"><i class="icon-logout">
                            </i><span>Logout</span>
                        </a>
                    </form>
                </li>
            </ul>
		</li>	
		<!-- END USER DROPDOWN -->
		<!-- CHAT BAR ICON -->
        <li id= "notification-header">
            <a href="<?php echo site_url('/transaction/checkout') ?>"  >
                <i class="icon-basket" style="font-size: 20px"></i> 
                <span data-rel="tooltip" class="badge badge-primary badge-header" data-toggle="tooltip" data-placement="bottom" title="Checkout" >
                      <span id="update_cart"><?php if($update_cart){echo array_sum($update_cart);} else { echo '0'; } ?></span>
                </span>
            </a>
            
        </li>

		<li id="quickview-toggle">
			<a href="#">
				<i class="icon-bell"></i>
				<span  class="badge badge-primary badge-header" id="notif-count">
                </span>
			</a>
		</li>

		<!-- END CHAT BAR ICON -->
	</ul>
</div>

<script type="text/javascript">
function submitform()
{
    document.forms["myform1"].submit();
}
</script>