<div class="quickview-header">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#chat" data-toggle="tab">Notification</a></li>
	</ul>
</div>

<div class="quickview">
	<div class="tab-content">
		<div class="tab-pane fade active in" id="chat">
			<div class="chat-body current">
				<div class="chat-groups">
					<div class="title"> <a href="<?php echo site_url('profile/notification')?>">Request Notification</a></div>
					<ul id="notif-content">
						<!-- <li><i class="turquoise"></i> Notification 1</li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>