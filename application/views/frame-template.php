<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?> | GAMABOX Enterprise</title>
        <?php $this->load->view('partial/head_script');?>
        <!-- css in selected pages -->
 		<?php echo $content_css; ?>
 		<!-- end of css in selected pages -->
        
	</head>

	<body  class="fixed-topbar fixed-sidebar theme-sdtl color-primary">
		<section>
			<!--README BEGIN SIDEBAR -->
			<div class="sidebar">
				<?php $this->load->view('partial/sidebar'); ?>	
			</div>	
			<!-- END SIDEBAR -->

			<!-- BEGIN MAIN CONTENT -->
			<div class="main-content">
				<!-- README BEGIN TOPBAR -->
				<div class="topbar">
					<?php $this->load->view('partial/header'); ?>
				</div>
				<!-- END TOPBAR -->

				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content page-thin" >
					
						<?php echo $content; ?>
					
					
					<div class="footer">
		            	<div class="copyright">
		              		<p class="pull-left sm-pull-reset">
		               			<span>Copyright <span class="copyright">©</span> 2015 </span>
		               			<span>GAMABOX Enterprise.</span>
		                		<span>All rights reserved.</span>
		             		</p>
		             		<p class="pull-right sm-pull-reset">
		               			<span><a href="" class="m-r-10">Support</a> | <a href="<?php echo site_url('dashboard/termofuse');?>" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
		              		</p>
		            	</div>
		         	</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END MAIN CONTENT -->
		</section>

		<!-- BEGIN QUICKVIEW SIDEBAR -->
		<div id="quickview-sidebar">
			<!-- show notification -->
			<?php $this->load->view('partial/notification'); ?>
			
		</div>
		<!-- END QUICKVIEW SIDEBAR -->

		<!-- BEGIN SEARCH -->
		<div id="morphsearch" class="morphsearch">
			<!-- show search page -->
			<?php $this->load->view('partial/search'); ?>
		</div>
		<!-- END SEARCH -->

		<!-- BEGIN PRELOADER -->
	    <div class="loader-overlay">
	      <div class="spinner">
	        <div class="bounce1"></div>
	        <div class="bounce2"></div>
	        <div class="bounce3"></div>
	      </div>
	    </div>
	    <!-- END PRELOADER -->

	     <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 

	    <!-- BEGIN SCRIPT ALL PAGES --> 
	    <?php $this->load->view('partial/footer_script');?>
	    <!-- END SCRIPT ALL PAGES -->
	    <!-- BEGIN SCRIPT FUNCTION -->
	    <?php echo $content_js; ?>
	    <!-- END SCRIPT FUNCTION -->
	</body>
</html>

