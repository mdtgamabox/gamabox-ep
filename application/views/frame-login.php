<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <!-- BEGIN CSS SCRIPT -->
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/ui.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/plugins/bootstrap-loading/lada.min.css'); ?>" rel="stylesheet">
        <!-- END CSS SCRIPT -->
    </head>

    <body class="account" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <?php echo $content; ?>
            <p class="account-copyright">
                <span>Copyright © 2015 </span><span>Gamabox Enterprise</span>.<span>All rights reserved.</span>
            </p>

            
        </div>
        <!-- END LOGIN BOX -->
        <script src="<?php echo site_url('assets/js/pages/login-v1.js') ?>"></script>
         <script src="<?php echo site_url('assets/plugins/bootstrap-loading/lada.min.js') ?>"></script>

    </body>
</html>