<?php 
	class Auth_lib{
		private $CI;

		public function __construct(){
			 $this-> CI =& get_instance();
		}


		function domain(){
			$url = 'http://192.168.0.201:8080';
			return $url;
		}

		function url(){
			$url = 'http://192.168.0.201:8080/enterprise-core/v1';
			return $url;
		}

		function logout(){		
			$url = 'http://192.168.0.201:8080/enterprise-core/logout';
			return $url;
		}

	
		
		/*method to get data*/
		function profile($session_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			
            $send = stream_context_create($request);

            $url = $domain.'/user/'.$session_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data; 
		}

		function avatar($session_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

			$url = $domain.'/avatar/user/'.$session_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data; 
		}

		function business_associate($ba_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

			$url = $domain.'/avatar/business_associate/'.$ba_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data; 
		}

		function get_document($session_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/document';
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_detail_document($documentID){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/document/'.$documentID;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}
	
		function get_request_detail($request_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/'.$request_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}


		function get_request_step($type_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/type/step/'.$type_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_request_status($request_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/'.$request_id."/status";
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}


		function get_request_user($session_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/user/'.$session_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_request_document($request_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/'.$request_id.'/document';
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_approval_user($session_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/approval/user/'.$session_id;
            
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_document_user($request_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/document/request/'.$request_id;
            	
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_document_request($request_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/request/'.$request_id.'/document';
            	
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_all_warehouse(){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/warehouse';
            	
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_warehouse_detail(){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/warehouse/detail';
            	
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}

		function get_warehouse_document($warehouse_id){
			$domain = $this->url();

			$request['http']['method'] = "GET";
			$send = stream_context_create($request);

            $url = $domain.'/document/warehouse/'.$warehouse_id;
            	
            $get=file_get_contents($url, true, $send);
            $data=json_decode($get);
            
            return $data;
		}


	}

?>
