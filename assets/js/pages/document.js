function list_document(selector){
	str='';
	var address_list_document = base_api+'/document';
    $.ajax({
        url: address_list_document,
        dataType: 'json',
        async: false,
        beforeSend: function() { $(selector).html('wait.....'); },
        success: function(data) {
            $(selector).empty();
            if (data.documentProfiles.length < 1) {
                str += 'No Data to show'
            } else {
                str += '<table id="well-document" class="table"> <thead><tr><th class="text-center">doctypeId</th><th class="text-center">Date</th><th class="text-center">Document Title</th><th class="text-center">Document Type</th><th class="text-center">Physical Data</th><th class="text-center">Digital Data</th><th class="text-center">Owner</th><th class="text-center">Order</th></tr></thead><tfoot><tr><th class="text-center">doctypeId</th><th class="text-center">Date</th><th class="text-center">Document Title</th><th class="text-center">Document Type</th><th class="text-center">Physical Data</th><th class="text-center">Digital Data</th><th class="text-center">Owner</th><th class="text-center">Order</th></tr></tfoot>';
                $.each(data.documentProfiles, function(key,val){
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                    var doctypename = val.docTypeName.split(";");
                    doctypestrname = '<ul>';
                    for (var x in doctypename){
                        doctypestrname += '<li>'+doctypename[x]+'</li>';
                    }
                    doctypestrname += '</ul>';
                    var doctype = val.docType.split(";");
                    doctypestr = "/";
                    for (var x in doctype){
                        doctypestr += doctype[x]+'/';
                    }
                    str += '<tr>';
                    str += '<td class="text-center">'+doctypestr+'</td>';
                    str += '<td>'+val.dayOfEffectiveDate+' '+months[val.monthOfEffectiveDate-1]+' '+val.yearOfEffectiveDate+'</td>';
                    str += '<td class="text-center"><a target="_blank" href="'+base_apps_url+'/profile/document/'+val.documentId+'">'+val.documentName+'</a></td>';
                    str += '<td class="text-center">'+doctypestrname+'</td>';
                    str += '<td class="text-center">'+val.warehouseId+'</td>';
                    str += '<td class="text-center">'+val.digitalType+'</td>';
                    str += '<td class="text-center">'+val.baName+'</td>';
                    if (array_cart.indexOf(val.documentId) >= 0) {
                        str += '<td class="text-center">'+'<form action="#" method="POST" class=""><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-danger disabled transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Requested</button></form>'+'</td>';
                    } else {
                        str += '<td class="text-center">'+'<form action="#" method="POST" class="transaction-add-doc"><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-primary transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Request</button></form>'+'</td>';
                    };
                    str += '</tr>';
                });
                str += '</table>'
                $(selector).html(str);    
            };
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() { 
        	
            var welldocument_table = $('#well-document').DataTable({
            	"iDisplayLength": 50,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false
                    }
                ],   
            });
            $("#tree-doctype").jstree("refresh");
                $("#tree-doctype").jstree("deselect_all");
                $("#tree-doctype").on("changed.jstree", function (e, data) {
                    // console.log(data.selected);
                    var ulala = '/'+data.selected+'/';
                    if (data.selected == 'all') {ulala = '/'};
                    welldocument_table.column(0).search( ulala, true, true, true ).draw();
                });
             
            
            $(document).on("submit" , '.transaction-add-doc' , function(event) {
                var $form = $(this);
                var formData = {
                    'document': $('input', $form).serialize(),
                };
                $.ajax({
                    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url         : base_apps_url+'/transaction/add_cart', // the url where we want to POST
                    data        : formData, // our data object
                    dataType    : 'json', // what type of data do we expect back from the server
                    encode      : true,
                    success : function(data) {
                     console.log(formData);
                     console.log(data);
                        $("#update_cart").html(data.update_cart); 
                        $('button', $form).removeClass().addClass('btn btn-danger disabled').html('<span class="glyphicon glyphicon-shopping-cart"></span> Requested')
                    },
                    error: function(jqXHR, textStatus, errorThrown) { console.log(JSON.stringify(formData)+' | '+textStatus+' | '+errorThrown)},
                    complete : function(){}
                })
                event.preventDefault();
            });
        }
    });
}

function tree_view_doctype(selector){
    console.log('asdf');
    var address_tree_document = base_api+'/doctype';
    var array_doctype = [{"id" : "all", "parent":"#", "text": "All", "icon": "fa fa-folder"}];
    $.ajax({
        url: address_tree_document,
        dataType: 'json',
        async: false,
        beforeSend: function() {  $(selector).html('udah destroy');},
        success: function(data) {
            $.each( data, function( key, val ) {
                $.each( val, function( keyx, valx ) {
                    if (!valx.doctypeGroup) {
                        parentDoctype = "#";
                        icon = false;
                    } else { 
                        parentDoctype = valx.doctypeGroup.doctypeId;
                        icon = "fa fa-folder"
                    };
                    var doctype_object = {
                        "id" : valx.doctypeId,
                        "parent" : parentDoctype,
                        "text" : valx.documentType,
                        "icon" : icon
                    }
                    array_doctype.push(doctype_object);
                });
            });
            $(selector).jstree({ 
                'core' : {
                    'data' : array_doctype
                } 
            });
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() { 
            // $("#tree-doctype-well").on("changed.jstree", function (e, data) {
            //     console.log(data.selected);
            //     welldocument_table.search( this.value ).draw();
            // });
        }
    }); 
}
$(function() {
	get_array_cart();
	list_document('#list-of-document');
    tree_view_doctype('#tree-doctype')
	$('#navdocument').addClass('nav-active active');
});