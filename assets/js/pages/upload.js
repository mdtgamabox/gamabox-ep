function user_uploaded_document(selector){
	var address_template = base_api+'/document/user/'+userId;
	str = '';
	number = 1;
	$.ajax({
        url: address_template,
        dataType: 'json',
        async: false,
        beforeSend: function() { $(selector).html('wait.....'); },
        success: function(data) {
        	// console.log(data);
        	str += '<table class="table table-dynamic"><thead><tr><th class="text-center">No</th><th class="text-center">Doc ID</th><th class="text-center">Document Name</th><th class="text-center">Document Type</th><th class="text-center">Doc Digital</th><th class="text-center">Doc Physical</th></tr></thead><tbody>' 
            $.each(data.documentProfiles, function(key,val){
                str += '<tr>';
                str += '<td class="text-center">'+number+'</td>';
                str += '<td class="text-center">'+val.documentId+'</td>';
                str += '<td class="text-center">'+val.documentName+'</td>';
                str += '<td class="text-center">'+val.docTypeName+'</td>';
                if (val.isDigital == 1) {docDigital = val.docSize} else {docDigital = '-'};
                str += '<td class="text-center">'+docDigital+'</td>';
                if (val.isPhysical == 1) {docPhysical = val.warehouseId} else {docPhysical = '-'};
                str += '<td class="text-center">'+docPhysical+'</td>';
                str += '</tr>';
                number++;
            });
            str += '</tbody></table>'
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() { $(selector).html(str); }
    }); 
	// $(selector).html(str);
}

function form_upload(selector_document_type){
	str = '';
	$.ajax({
		url: base_api+'/doctype',
		dataType: 'json',
		success: function(data) {
            $.each(data.doctypes, function(key,val){
            	// console.log(val.doctypeId+' '+val.documentType);
            	str += '<option value="'+val.doctypeId+'">'+val.documentType+'</option>';
            });
        },
        error: function(jqXHR, textStatus, errorThrown) { str += jqXHR},
        complete : function(){$(selector_document_type).html(str);}
	});
}

function template_table(selector){
	var address_template = base_api+'/template';
	str = '';
	number = 1;
	$.ajax({
        url: address_template,
        dataType: 'json',
        async: false,
        beforeSend: function() { $(selector).html('wait.....'); },
        success: function(data) {
        	str += '<table class="table table-dynamic"><thead><tr><th class="text-center">No</th><th class="text-center">Template File</th><th class="text-center">Type</th></tr></thead><tbody>' 
            $.each(data.templates, function(key,val){
                str += '<tr>';
                str += '<td class="text-center">'+number+'</td>';
                str += '<td><a href="'+server_address+val.path+'" download>'+val.templateName+'</a></td>';
                str += '<td>'+val.templateType.documentType+'</td>';
                str += '</tr>';
                number++;
            });
            str += '</tbody></table>'
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() { $(selector).html(str); }
    }); 
	// $(selector).html(str);
}

template_table('#template_table');
user_uploaded_document('#uploaded-document')
form_upload('#option-upload')
i = 0


var stepped = 0, chunks = 0, rows = 0;
var start, end;
var parser;
var pauseChecked = false;
var printStepChecked = false;


function errorFnGamabox(error, file){
	console.log("ERROR:", error, file);
}
function completeFnGamabox(){
	//README - bikin value untuk form | content aja tanpa header, delimiter ; untuk kolom, delimiter & untuk beda baris
	array_string = [];
	$.each(arguments[0].data, function(head, content) {
		string_form = '';
		string_tes = [];
		$.each(content, function(key, value) {
			string_tes.push(value);
		});
		string_form = string_tes.join(';')
		array_string.push(string_form);
	});
	array_string = array_string.join('&');
	$('#value-form').attr('value', array_string);
	$('#fileName-form').attr('value', filename);

	//README - send form via AJAX - tanpa ngirim file
	// address_post = base_api+'/submit/batch/'+doctype;
	// $.ajax({
	// 	type: "POST",
	// 	url: address_post,
	// 	data: {
	//   		'_csrf': token, 
	//   		'value' : array_string
	//   	},
	// 	xhrFields: {
	// 		withCredentials: true
	// 	},
	// 	success: function(data){
	//   		console.log(data);
	//   	},
	//   	error: function(jqXHR, textStatus, errorThrown) { console.log(textStatus+' | '+errorThrown)},
	// });

	// README - printing after parse table
	str = '<ul><li>File Name : '+filename+'</li></ul><br><table class="table" id="table'+i+'"><thead><tr>';
	$.each(arguments[0].data[0], function(head, content) {
		str += '<th>'+head+'</th>' 
	});
	str += '</tr></thead><tbody>';
	$.each(arguments[0].data, function(i, el) {
		str += '<tr>'
		$.each(el, function(j, cell) {
			str += '<td>'+cell+'</td>'
		});
		str += '</tr>'
	});
	str += '</tbody></table>';
	//str += '<form action="'+base_api+'/submit/template" method="post"><input type="hidden" name="_csrf" value="'+token+'"><input type="hidden" name="template" value="'+array_string+'"><input type="submit" value="Submit form"></form><hr>' 
	$("#afterparse-table-gamabox").prepend(str);
	table = '#table'+i;
	var oTable = $(table).dataTable({
		"scrollX": true
    });
	i++;
	$('#check-parse-gamabox').hide()
	$('#submit-form').show();
    // $("#files-gamabox").val('');
}

$(function(){
	$('#submit-form').hide();
	$('#check-parse-gamabox').click(function(){
		stepped = 0;
		chunks = 0;
		rows = 0;
		console.log($('#files-gamabox'));
		var files = $('#files-gamabox')[0].files;
		doctype = files[0].name.split('.')[0];
		filename = files[0].name;
		$('#form-batch-upload').attr('action', base_api+'/submit/batch/'+doctype+'/'+window.location.hostname+'/gamabox-ep/batch_upload');
		// $('#form-batch-upload').attr('action', 'http://localhost/POST.PHP');


		var config = {
			delimiter: "",	// auto-detect
			newline: "",	// auto-detect
			header: true,
			skipEmptyLines: true,
			complete: completeFnGamabox,
			error: errorFnGamabox
		}

		$('#files-gamabox').parse({
			config: config,
			before: function(file, inputElem) {
				// console.log("Parsing file:", file);
			},
			complete: function() {
				// console.log("Done with all files.");
			}
		});

	});
	//Sidebar active indicator
    $('#navupload').addClass('nav-active active');
});


function stepFn(results, parserHandle) {
	stepped++;
	rows += results.data.length;

	parser = parserHandle;
	
	if (pauseChecked)
	{
		console.log(results, results.data[0]);
		parserHandle.pause();
		return;
	}
	
	if (printStepChecked)
		console.log(results, results.data[0]);
}

function chunkFn(results, streamer, file) {
	if (!results)
		return;
	chunks++;
	rows += results.data.length;

	parser = streamer;

	if (printStepChecked)
		console.log("Chunk data:", results.data.length, results);

	if (pauseChecked)
	{
		console.log("Pausing; " + results.data.length + " rows in chunk; file:", file);
		streamer.pause();
		return;
	}
}
