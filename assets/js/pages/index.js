$(function() {


    $(document).on("click", ".panel-header .panel-maximize", function(event) {
        var panel = $(this).parents(".panel:first");
        if (panel.hasClass("maximized")) {
            map.invalidateSize();
        }
        else {
            map.invalidateSize();
        }
    });
    $(document).on("click", "#switch-rtl", function(event) {
        map.zoomControl = generateZoomControl();
        map.validateData();
        $('#listdiv').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            scrollInertia: 150,
            theme: "light-thin",
            set_height: 440,
            advanced: {
                updateOnContentResize: true
            }
        });
    });


    /* Notifications, demo purpose */
    notifContent = '<div class="alert alert-dark media fade in bd-0" id="message-alert"><div class="media-left"></div><div class="media-body width-100p"><h4 class="alert-title f-14">Latest Notification</h4><p class="f-12 alert-message pull-left">Notification 1</p><p class="pull-right"><a href="#" class="f-12">Open Notification</a></p></div></div>';
    setTimeout(function() {
        if (!$('#quickview-sidebar').hasClass('open') && !$('.page-content').hasClass('page-builder') && !$('.morphsearch').hasClass('open')) generateNotifDashboard(notifContent);
    }, 3000);

    // panel-stat-chart, visitors-chart
    var widgetMapHeight = $('.widget-map').height();
    var pstatHeadHeight = $('.panel-stat-chart').parent().find('.panel-header').height() + 12;
    var pstatBodyHeight = $('.panel-stat-chart').parent().find('.panel-body').height() + 15;
    var pstatheight = widgetMapHeight - pstatHeadHeight - pstatBodyHeight + 30;
    $('.panel-stat-chart').css('height', pstatheight);
    var clockHeight = $('.jquery-clock ').height();
    var widgetProgressHeight = $('.widget-progress-bar').height();
    $('.widget-progress-bar').css('margin-top', widgetMapHeight - clockHeight - widgetProgressHeight - 3);
    
    //README Grafik last 12 month production
    var address_all_production_12months = base_api+"/production/well/all/last-12-month"
    $.getJSON( address_all_production_12months, function( data ) {
        var sumNglVolume = [];
        var sumOilVolume = [];
        var sumGasVolume = [];
        var ProductionMonth = [];
        var jumlahOil = 0;
        var jumlahGas = 0;
        var monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct','Nov', 'Dec'];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                monthNumber = valx.month - 1;
                monthNameProduction = monthName[monthNumber];
                ProductionMonth.push(monthNameProduction+"-"+valx.year);
                sumNglVolume.push(valx.sumNglVolume);
                sumOilVolume.push(valx.sumOilVolume);
                sumGasVolume.push(valx.sumGasVolume);
                jumlahOil += valx.sumOilVolume;
                jumlahGas += valx.sumGasVolume;
              });
          });
        $( "#jumlahOil" ).html( jumlahOil );
        $( "#jumlahGas" ).html( jumlahGas );
        var visitorsData = {
            labels: ProductionMonth,
            datasets: [
                {
                    label: "Oil (Bbl)",
                    fillColor: "rgba(200,200,200,0.5)",
                    strokeColor: "rgba(200,200,200,1)",
                    pointColor: "rgba(200,200,200,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(200,200,200,1)",
                    data: sumOilVolume
                },
                {
                    label: "NGL (Bbl)",
                    fillColor: "rgba(49, 157, 181,0.1)",
                    strokeColor: "rgba(49, 157, 181,0.7)",
                    pointColor: "rgba(49, 157, 181,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(49, 157, 181,1)",
                    data: sumNglVolume
                },
                {
                    label: "Gas (MCFD)",
                    fillColor: "rgba(232, 44, 12,0.1)",
                    strokeColor: "rgba(232, 44, 12,0.7)",
                    pointColor: "rgba(232, 44, 12,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(232, 44, 12,1)",
                    data: sumGasVolume
                }
            ]
        };
        var chartOptions = {
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            pointDot: true,
            pointHitDetectionRadius: 20,
            tooltipCornerRadius: 0,
            scaleShowLabels: false,
            tooltipTemplate: "dffdff",
            multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
            responsive: true,
            scaleShowLabels: false,
            showScale: false,
        };
        var ctx = document.getElementById("monthproduction-chart").getContext("2d");
        var myNewChart = new Chart(ctx).Line(visitorsData, chartOptions);
    });
    
    //README Grafik Warehouse Statistics
    var address_all_production_12months = base_api+"/production/well/all/last-12-month"
    $.getJSON( address_all_production_12months, function( data ) {
        
        var pieData = [
              {value: 300, color:"rgba(54, 173, 199,0.9)", highlight: "rgba(54, 173, 199,1)", label: "Geophysics Data"},
              {value: 40, color: "rgba(201, 98, 95,0.9)", highlight: "rgba(201, 98, 95,1)", label: "Geological Data"},
              {value: 100,color: "rgba(255, 200, 112,0.9)", highlight: "rgba(255, 200, 112,1)", label: "Geomathic Data"}
          ];
        var chartOptions = {
            tooltipCornerRadius: 0,
            responsive: true,
        };
        var ctx = document.getElementById("warehouse-chart").getContext("2d");
        var myNewChart = new Chart(ctx).Pie(pieData, chartOptions);
    });

    //README Chart Production Multi Axes
    //README GET JSON & Print Chart
    var address_all_production = base_api+'/production/well/all/monthly';
    $.getJSON( address_all_production, function( data ) {
      var sumNglVolume = [];
      var sumOilVolume = [];
      var sumWaterVolume = [];
      var sumGasVolume = [];
      var ProductionMonth = [];
      var sumGrossVolume = [];
      var monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct','Nov', 'Dec'];
      $.each( data, function( key, val ) {
        $.each( val, function( keyx, valx ) {
            monthNumber = valx.month - 1;
            monthNameProduction = monthName[monthNumber];
            ProductionMonth.push(monthNameProduction+"-"+valx.year);
            sumNglVolume.push(valx.sumNglVolume);
            sumOilVolume.push(valx.sumOilVolume);
            sumWaterVolume.push(valx.sumWaterVolume);
            sumGasVolume.push(valx.sumGasVolume);
            sumGrossVolume.push(valx.sumWaterVolume + valx.sumOilVolume + valx.sumNglVolume)
          });
      });
        $('#production-chart').highcharts({
            chart: {
                zoomType: 'x'
            },
            navigator: {
                enabled: true
            },
            scrollbar: {
              enabled: true
            },
            credits: {
              enabled: false
            },
            title: {
                text: 'Well Production PERTAMINA'
            },
            subtitle: {
                text: 'Source: Pertamina EP'
            },
            xAxis: [{
                categories: ProductionMonth,
                crosshair: true,
                labels :{
                    rotation: -45
                }
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} MCFD',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                title: {
                    text: 'Gas (MCFD)',
                    style: {
                        color: Highcharts.getOptions().colors[2]
                    }
                },
                opposite: true

            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Gross, Oil, Water, NGL (Bbl)',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} Bbl',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }

            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                x: 0,
                verticalAlign: 'bottom',
                y: 0,
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'NGL',
                type: 'spline',
                yAxis: 1,
                data: sumNglVolume,
                tooltip: {
                    valueSuffix: ' Bbl'
                }

            }, {
                name: 'Oil',
                type: 'spline',
                yAxis: 1,
                data: sumOilVolume,
                tooltip: {
                    valueSuffix: ' Bbl'
                }

            }, {
                name: 'Water',
                type: 'spline',
                yAxis: 1,
                data: sumWaterVolume,
                marker: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Bbl'
                }

            }, {
                name: 'Gross',
                type: 'spline',
                yAxis: 1,
                data: sumGrossVolume,
                marker: {
                    enabled: false
                },
                dashStyle: 'shortdot',
                tooltip: {
                    valueSuffix: ' Bbl'
                }

            }, {
                name: 'Gas',
                type: 'spline',
                data: sumGasVolume,
                tooltip: {
                    valueSuffix: ' MCFD'
                }
            }]
        });
    });
    

    /* Progress Bar  Widget */
    if ($('.widget-progress-bar').length) {
        $(window).load(function() {
            setTimeout(function() {
                $('.widget-progress-bar .stat1').progressbar();
            }, 900);
            setTimeout(function() {
                $('.widget-progress-bar .stat2').progressbar();
            }, 1200);
            setTimeout(function() {
                $('.widget-progress-bar .stat3').progressbar();
            }, 1500);
            setTimeout(function() {
                $('.widget-progress-bar .stat4').progressbar();
            }, 1800);
        });
    };

    var address_all_well = base_api+"/well"
    $.getJSON( address_all_well, function( data ) {
        var markers = [];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                markers.push([valx.surfaceLongitude, valx.surfaceLatitude, valx.wellName, valx.uwi])
            });
        });

        // README init mapzz
        var map = new ol.Map({
            target: 'map-openlayer',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.MapQuest({layer: 'osm'}) // Street map -> osm
                })
            ],
            // post on map
            view: new ol.View({
                center: ol.proj.fromLonLat([116.97,-2.56]),
                zoom: 5
            })
        });
        var vectorSource = new ol.source.Vector({
            //create empty vector
        });
        // var markers = [];
        function AddMarkers() {
            // var markers = [
            //    [ 95.90306, 2.98917 , "SIMEULUE"],
            //    [ 110.41, -7.73, "<h2>Well 2</h2>"],
            //    [103.49, -0.35, "<h2>Well 3</h2>" ],
            //    [111.99, -8.04, "<h2>Well 4</h2>"],
            //    [101.51,  -0.93, "<h2>Well 5</h2>"],
            //    [107.68, -1.16, "<h2>Well 6</h2>"]
            // ];
            var latlong_array_length = markers.length;
            //create a bunch of icons and add to source vector
            for (var i=0;i<latlong_array_length;i++){
                var lon = markers[i][0];
                var lat = markers[i][1];
                var iconFeature = new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.fromLonLat([lon,lat])),
                    name: '<a href="'+base_apps_url+'/profile/well/'+markers[i][3]+'">'+markers[i][2]+'</a>'  ,
                });
                vectorSource.addFeature(iconFeature);
            }

            //create the style
            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                    anchor: [0.5, 46],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    opacity: 0.75,
                    src: 'assets/images/map-marker.png'
                }))
            });
            //add the feature vector to the layer vector, and apply a style to whole layer
            var vectorLayer = new ol.layer.Vector({
                    source: vectorSource,
                    style: iconStyle
                });
                return vectorLayer;
        }

        var element = document.getElementById('popup');
        var popup = new ol.Overlay({
          element: element,
          positioning: 'bottom-center',
          stopEvent: false
        });
        map.addOverlay(popup);
        // display popup on click
        map.on('click', function(evt) {
          var feature = map.forEachFeatureAtPixel(evt.pixel,
              function(feature, layer) {
                return feature;
              });
          if (feature) {
            popup.setPosition(evt.coordinate);
            $(element).popover({
              'placement': 'top',
              'html': true,
              'content': feature.get('name')
            });
            $(element).popover('show');
          } else {
            $(element).popover('destroy');
          }
        });
        var layerMarkers = AddMarkers();
        map.addLayer(layerMarkers);

    });
    $('#navhome').addClass('active');
    
   

});

function generateNotifDashboard(content) {
    var position = 'topRight';
    if ($('body').hasClass('rtl')) position = 'topLeft';
    var n = noty({
        text: content,
        type: 'success',
        layout: position,
        theme: 'made',
        animation: {
            open: 'animated bounceIn',
            close: 'animated bounceOut'
        },
        timeout: 1500,
        callback: {
            onShow: function() {
                $('#noty_topRight_layout_container, .noty_container_type_success').css('width', 350).css('bottom', 10);
            },
            onCloseClick: function() {
                setTimeout(function() {
                    $('#quickview-sidebar').addClass('open');
                }, 500)
            }
        }
    });
}