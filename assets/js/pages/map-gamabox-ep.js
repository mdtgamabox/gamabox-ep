function map_all(selector){
	var address_all_seis = base_api+'/seis';
	$.ajax({
        url: address_all_seis,
        dataType: 'json',
        async: false,
        beforeSend: function() { $(selector).html('wait.....'); },
        success: function(data) {
            // console.log(data);
            seismic_coordinates = [];
            var _lat = [];
            var _long = [];
            $.each( data.seisSets, function( key, val ) {
            	if (val.minLatitude&&val.minLongitude != null) {
                    _lat.push(val.minLatitude);_lat.push(val.maxLatitude);
                    _long.push(val.minLongitude);_long.push(val.maxLongitude)
            		seismic_coordinates.push([
						{lat: val.minLatitude, lng: val.minLongitude},
						{lat: val.maxLatitude, lng: val.maxLongitude}
					]);
            		// console.log("minLat "+val.minLatitude+" maxLat "+val.maxLatitude)
              //   	console.log("minLong "+val.minLongitude+" maxLong "+val.maxLongitude)
            	};
                center_lat = (Math.min.apply(Math,_lat)+Math.max.apply(Math,_lat))/2;
                center_long = (Math.min.apply(Math,_long)+Math.max.apply(Math,_long))/2;
            });
            // console.log(seismic_coordinates);
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() {
        	// console.log("ulala");
        }
    });
    center_map = {lat: center_lat, lng: center_long};
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: center_map
        });
        var contentString = '<strong><a target="_blank" href="'+base_apps_url+'/profile/seismic/'+this.seismicId+'/'+this.seismicType+'"> '+this.seismicId+'</a></strong> | '+this.seismicType;
        // console.log(seismic_coordinates);
        for (var i = 0; i < seismic_coordinates.length; i++) {
            // console.log(seismic_coordinates[i]);
            var seismictPath = new google.maps.Polyline({
            path: seismic_coordinates[i],
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            map: map
          });
        }
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        // var marker = new google.maps.Marker({
        //     position: center_map,
        //     map: map,
        //     title: this.seismicId
        // });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
	// string = "ulalalalal";
	// $(selector).html(string);
}
$(function() {
	map_all('#map-seismic');
});