function detail_notification(){
    var str ='';
    var address_get_notification = base_api+'/notification';
    $.ajax({
        url: address_get_notification,
        dataType: 'json',
        xhrFields: {
                withCredentials: true
            },
        success : function(data){
            $.each(data.notifications, function(key,val) {
                if (val.status == 'Unread') {
                    notif_status = '<h4 class="text-danger">Notification ID : '+val.notificationId+'</h4>'
                    $.ajax({
                        url :base_api+'/notification/'+val.notificationId+'/read',
                        xhrFields : {
                            withCredentials: true
                        },
                        success : function(data){
                            console.log('success');
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            console.log(JSON.stringify(jqXHR)+' '+textStatus+' '+errorThrown)             
                        }
                    });
                } else {
                    notif_status = '<h4>Notification ID : '+val.notificationId+'</h4>'
                };
                str += notif_status+'<table class="table table-striped"><tr><td><strong>Date : </strong>'+val.notifDate+'</td></tr><tr><td><strong>Request </strong>id : '+val.requestId.requestId+'| date : '+val.requestId.requestDate+'</td></tr><tr><td><strong>Content </strong>'+val.content+'</td></tr></table><hr>';
                
            });
            $('#notif-detail').html(str);
        },
        error: function(jqXHR, textStatus, errorThrown) { 
            console.log(JSON.stringify(jqXHR));
            console.log(jqXHR+' '+textStatus+' '+errorThrown) 
        }
    });
}

detail_notification();