function demoFromHTML(requestId) {
    var pdf = new jsPDF('p', 'pt', 'letter')
    , source = $('#fromHTMLtestdiv')[0]
    , specialElementHandlers = {
        '#bypassme': function(element, renderer){
            return true
        }
    }
    margins = {
      top: 80,
      bottom: 60,
      left: 40,
      width: 522
    };
    pdf.fromHTML(
        source 
        , margins.left
        , margins.top 
        , {
            'width': margins.width
            , 'elementHandlers': specialElementHandlers
        },
        function (dispose) {
          console.log(pdf);
          pdf.save('Transmittal'+requestId+'.pdf');
        },
        margins
    )
}

$(function() {
    //Get Global Variable
    get_array_cart();

    //README Panel Header - 
    $(document).on("click", ".panel-header .panel-maximize", function(event) {
        var panel = $(this).parents(".panel:first");
        if (panel.hasClass("maximized")) {
            map.invalidateSize();
        }
        else {
            map.invalidateSize();
        }
    });
    $(document).on("click", "#switch-rtl", function(event) {
        map.zoomControl = generateZoomControl();
        map.validateData();
        $('#listdiv').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            scrollInertia: 150,
            theme: "light-thin",
            set_height: 440,
            advanced: {
                updateOnContentResize: true
            }
        });
    });

    /* Progress Bar  Widget */
    if ($('.widget-progress-bar').length) {
        $(window).load(function() {
            setTimeout(function() {
                $('.widget-progress-bar .stat1').progressbar();
            }, 900);
            setTimeout(function() {
                $('.widget-progress-bar .stat2').progressbar();
            }, 1200);
            setTimeout(function() {
                $('.widget-progress-bar .stat3').progressbar();
            }, 1500);
            setTimeout(function() {
                $('.widget-progress-bar .stat4').progressbar();
            }, 1800);
        });
    };
   
    //README - Kalau Profile Well
    if (typeof(uwi) != "undefined"){
        treeview_doctype_well();
        var well = new Well(uwi);
        $( "#panel-well" ).removeClass("hide");
        well.print_head_properties('#TitleWell');
        well.print_properties('#tab1_1');
        well.print_production('#chart-wellproduction', '#table-wellproduction-container');
        well.print_table_document('#table-welldocument');
        well.print_avatar('#avatar-well');
        well.print_well_activity('#table-wellactivityx');
    } else if (typeof(fieldId) != "undefined") {
        var field = new Field(fieldId);
        field.print_head_properties('#TitleField');
    } else if (typeof(areaId) != "undefined") {
        var area = new Area(areaId);
        area.print_head_properties('#TitleArea');
    } else if (typeof(warehouseId) != "undefined") {
        var warehouse = new Warehouse(warehouseId);
        warehouse.print_head_properties('#TitleWarehouse');
    } else if (typeof(documentId) != "undefined") {
        var document = new Document(documentId);
        document.print_head_properties('#TitleDocument');
        document.print_properties('#tab1');
        document.print_preview_document('#tab2');
    } else if (typeof(seismicId) != "undefined") {
        var seismic = new Seismic(seismicId, seismicType);
        seismic.print_head_properties('#TitleSeismic');
        seismic.print_properties('#seismic-tab1');
        seismic.print_map('#seismic-tab2');
        seismic.print_seis_document('#seismic-tab3');
        seismic.print_seis_data('#seismic-tab4');
    } else if(typeof(requestId) != "undefined"){
        bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        // console.log(requestId);
        $.ajax({
            url: base_api+'/request/'+requestId,
            async: false,
            dataType: 'json',
            success: function(data){
                // console.log(data);
                status_request = data.status;
                request_profile = data;
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR+' '+textStatus+' '+errorThrown)
            }
        });
        if (status_request == 'Accepted') {
            $.ajax({
                url: base_api+'/request/'+requestId+'/document',
                dataType: 'json',
                success: function(data){
                    var i = 1
                    str = '';
                    tanggal_request = request_profile.requestDate.split('-');
                    // console.log(tanggal_request);
                    $('#detil-permohonan').html('<li>Pembuat Permohonan : '+request_profile.requestor.userName+'</li><li>Perusaaan Pembuat Permohonan : '+request_profile.requestor.businessAssociate.baName+'</li><li>Tanggal Permohonan : '+tanggal_request[2]+'/'+bulan[tanggal_request[1]]+'/'+tanggal_request[0]+'</li><li>Tujuan Permohonan : '+data.documentsByRequest[0].documentId.businessAssociate.baName+'</li>');
                    $.each(data.documentsByRequest, function(key,val){
                        str += '<tr><td>'+i+'</td><td>'+val.documentId.documentName+'</td><td>'+val.documentId.isPhysical+'</td><td>'+val.documentId.isDigital+'</td></tr>';
                        i++;
                    });
                    $('#tabel-dokumen').html(str);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR+' '+textStatus+' '+errorThrown)
                }
            });
            //detil approval
            $.ajax({
                url: base_api+'/approval/request/'+requestId,
                dataType: 'json',
                success: function(data){
                    // console.log(data);
                    tanggal_pengesahan = data.approvals[0].approvalDate.split('-');
                    // console.log(tanggal_pengesahan);
                    $('#detil-pengesahan').html('<li>Tanggal Pengesahan : '+tanggal_pengesahan[2]+'/'+bulan[tanggal_pengesahan[1]]+'/'+tanggal_pengesahan[0]+'</li><li>Pemberi Pengesahan : '+data.approvals[0].target.businessAssociate.baName+'</li>');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR+' '+textStatus+' '+errorThrown)
                }
            });

        } else {
            alert('Mohon maaf request ini belum di approve, terima kasih');
        };
    };
});