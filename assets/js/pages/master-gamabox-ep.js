var server_address = "http://192.168.0.201:8080";
var api_version = "/enterprise-core/v1";
var base_api = server_address+api_version;
// var base_apps_url = 'http://'+window.location.hostname+'/gamabox-ep';
var timeout_core = 1800; // data on second

// README CLASS WELL
function Well(uwi){
    this.wellId = uwi;
    var address_well_profile = base_api+'/well/'+this.wellId;
    $.ajax({
        url: address_well_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            wellName = data.wells[0].wellName;
        },
        error: function(jqXHR, textStatus, errorThrown) { wellName = textStatus},
    }); 
    this.wellName = wellName;

    this.print_head_properties = function(selector) {
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/well/'+this.wellId+'">'+wellName+'</a></strong> | '+this.wellId);
    }
    this.print_properties = function(selector) {
        str='';
        $.ajax({
            url: address_well_profile,
            dataType: 'json',
            async: false,
            beforeSend: function() { $(selector).html('wait.....'); },
            success: function(data) {
                profileWell = data.wells[0];
                try {
                    baShortName = profileWell.operator.baShortName;
                }
                catch(err) {
                    baShortName = ' ';
                }
                str = '<div class="row"><div class="col-md-6"><ul><li>Well ID : '+profileWell.uwi+'</li><li>Well Name : '+profileWell.wellName+'</li><li>Owner : '+baShortName+'</li><li>RKB Elevation : </li><li>Water Depth : </li><li>Ground Level Elevation : </li><li>Total Depth : </li><li>True Vertical Depth : </li><li>Prognosed Depth : </li><li>Actual Depth : </li><li>Total Depth Logger : </li><li>Derrick Floor : </li><li>Location : </li><li>Well Alias : </li><li>Drill Type : </li><li>Well Status : </li><li>Well Remark : </li></ul></div><div class="col-md-6"><ul><li>Position : </li><li>Well Type : </li><li>Spud Date : '+profileWell.spudDate+'</li><li>Completion Date : </li><li>Seismic Line Reference : </li><li>AT Shotpoint : </li><li>Structure : </li><li>RIG : </li><li>Name/Rate/Capacity/Owner : </li><li>SP Name : </li><li>Datum : </li><li>CM - Central Meridian : </li><li>Surface Coordinate : </li><li>Longitude : '+profileWell.surfaceLongitude+'</li><li>Latitude : '+profileWell.surfaceLatitude+'</li></ul></div></div>'
            },
            error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
            complete: function() { $(selector).html(str); }
        }); 
    }
    this.print_production = function(selectorGraph, selectorTable) {
        str='';
        var widthProductionTab = $("#container-content").width();
        var address_well_production = base_api+'/production/well/'+this.wellId+'/monthly';
        $.ajax({
            url: address_well_production,
            dataType: 'json',
            async: false,
            beforeSend: function() { $(selectorGraph).html('wait.....'); },
            success: function(data) {
                if (data.pdenwells.length < 1) {
                    str = 'tidak ada yang ditampilkan';
                    $(selectorGraph).html(str);
                    $(selectorTable).html('');
                } else {
                    var str = '<h2 class="text-center">Production Table</h2><div id="renderingEngineFilter"></div><table class="table dataTable" id="table-wellproduction"><thead><tr><th class="text-center">Production Date</th><th class="text-center">Production Interval</th><th class="text-center">Gross (Bbl)</th><th class="text-center">Oil (Bbl)</th><th class="text-center">Gas (MCFD)</th><th class="text-center">Water (Bbl)</th><th class="text-center">NGL (Bbl)</th></tr></thead><tfoot><tr><th class="text-center">Production Date</th><th class="text-center">Production Interval</th><th class="text-center">Gross (Bbl)</th><th class="text-center">Oil (Bbl)</th><th class="text-center">Gas (MCFD)</th><th class="text-center">Water (Bbl)</th><th class="text-center">NGL (Bbl)</th></tr></tfoot><tbody>';
                    var nglVolume = [];
                    var oilVolume = [];
                    var waterVolume = [];
                    var gasVolume = [];
                    var ProductionMonth = [];
                    var sumGrossVolume = [];
                    var monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct','Nov', 'Dec'];
                    $.each(data.pdenwells, function(key,val){
                        dateProduction = val.volumeDate;
                        arrayDateProduction = dateProduction.split("/");
                        monthNumber = arrayDateProduction[0] - 1;
                        monthNameProduction = monthName[monthNumber];
                        ProductionMonth.push(monthNameProduction+"-"+arrayDateProduction[2]);
                        nglVolume.push(val.nglVolume);
                        oilVolume.push(val.oilVolume);
                        waterVolume.push(val.waterVolume);
                        gasVolume.push(val.gasVolume);
                        sumGrossVolume.push(val.waterVolume + val.oilVolume + val.nglVolume);
                        str += '<tr>';
                        str += '<td>'+monthNameProduction+'-'+arrayDateProduction[2]+'</td>';
                        str += '<td class="text-center">'+val.periodType+'</td>';
                        str += '<td class="text-center">'+Math.round((val.waterVolume + val.oilVolume + val.nglVolume)*100)/100+'</td>';
                        str += '<td class="text-center">'+val.oilVolume+'</td>';
                        str += '<td class="text-center">'+val.gasVolume+'</td>';
                        str += '<td class="text-center">'+val.waterVolume+'</td>';
                        str += '<td class="text-center">'+val.nglVolume+'</td>';
                        str += '</tr>';
                    });
                    $(selectorGraph).highcharts({
                        chart: {
                            zoomType: 'x',
                            width: widthProductionTab
                        },
                        navigator: {
                            enabled: true
                        },
                        scrollbar: {
                          enabled: true
                        },
                        credits: {
                          enabled: false
                        },
                        title: {
                            text: 'Well Production '+ wellName
                        },
                        subtitle: {
                            text: '--'
                        },
                        xAxis: [{
                            categories: ProductionMonth,
                            crosshair: true,
                            labels :{
                                rotation: -45
                            }
                        }],
                        yAxis: [{ // Primary yAxis
                            labels: {
                                format: '{value} MCFD',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            },
                            title: {
                                text: 'Gas (MCFD)',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            },
                            opposite: true

                        }, { // Secondary yAxis
                            gridLineWidth: 0,
                            title: {
                                text: 'Gross, Oil, Water, NGL (Bbl)',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value} Bbl',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            }

                        }],
                        tooltip: {
                            shared: true
                        },
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            x: 0,
                            verticalAlign: 'bottom',
                            y: 0,
                            floating: false,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                        },
                        series: [{
                            name: 'NGL',
                            type: 'spline',
                            yAxis: 1,
                            data: nglVolume,
                            tooltip: {
                                valueSuffix: ' Bbl'
                            }

                        }, {
                            name: 'Oil',
                            type: 'spline',
                            yAxis: 1,
                            data: oilVolume,
                            tooltip: {
                                valueSuffix: ' Bbl'
                            }

                        }, {
                            name: 'Water',
                            type: 'spline',
                            yAxis: 1,
                            data: waterVolume,
                            marker: {
                                enabled: false
                            },
                            tooltip: {
                                valueSuffix: ' Bbl'
                            }

                        }, {
                            name: 'Gross',
                            type: 'spline',
                            yAxis: 1,
                            data: sumGrossVolume,
                            marker: {
                                enabled: false
                            },
                            dashStyle: 'shortdot',
                            tooltip: {
                                valueSuffix: ' Bbl'
                            }

                        }, {
                            name: 'Gas',
                            type: 'spline',
                            data: gasVolume,
                            tooltip: {
                                valueSuffix: ' MCFD'
                            }
                        }]
                    });
                    str += '</tbody></table>';
                    $(selectorTable).html(str);
                    $('#table-wellproduction').dataTable()
                        .columnFilter({aoColumns:[
                            { sSelector: "#renderingEngineFilter", type:"select"  },
                             null ,
                             null ,
                            null ,
                            null 
                            ]}
                    );
                };
            },
            error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
            complete: function() {  
                // var table_production = $('#table-wellproduction').DataTable({
                // });
                
    
            }
        }); 
    }
    this.print_table_document = function(selector) {
        str='';
        var address_well_document = base_api+'/well/'+this.wellId+'/document';
        $.ajax({
            url: address_well_document,
            dataType: 'json',
            async: false,
            beforeSend: function() { $(selector).html('wait.....'); },
            success: function(data) {
                $(selector).empty();
                if (data.documentProfiles.length < 1) {
                    str += 'No Data to show'
                } else {
                    str += '<table id="well-document" class="table"> <thead><tr><th class="text-center">doctypeId</th><th class="text-center">Date</th><th class="text-center">Document Title</th><th class="text-center">Document Type</th><th class="text-center">Owner</th><th class="text-center">Order</th></tr></thead>';
                    $.each(data.documentProfiles, function(key,val){
                        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                        var doctypename = val.docTypeName.split(";");
                        doctypestrname = '<ul>';
                        for (var x in doctypename){
                            doctypestrname += '<li>'+doctypename[x]+'</li>';
                        }
                        doctypestrname += '</ul>';
                        var doctype = val.docType.split(";");
                        doctypestr = "/";
                        for (var x in doctype){
                            doctypestr += doctype[x]+'/';
                        }
                        str += '<tr>';
                        str += '<td class="text-center">'+doctypestr+'</td>';
                        str += '<td>'+val.dayOfEffectiveDate+' '+months[val.monthOfEffectiveDate-1]+' '+val.yearOfEffectiveDate+'</td>';
                        str += '<td class="text-center"><a target="_blank" href="'+base_apps_url+'/profile/document/'+val.documentId+'">'+val.documentName+'</a></td>';
                        str += '<td class="text-center">'+doctypestrname+'</td>';
                        str += '<td class="text-center">'+val.baName+'</td>';

                        if (array_cart.indexOf(val.documentId) >= 0) {
                            str += '<td class="text-center">'+'<form action="#" method="POST" class=""><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-danger disabled transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Requested</button></form>'+'</td>';
                        } else {
                            str += '<td class="text-center">'+'<form action="#" method="POST" class="transaction-add-doc"><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-primary transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Request</button></form>'+'</td>';
                        };
                        str += '</tr>';
                    });
                    str += '</table>'
                    $(selector).html(str);    
                };
            },
            error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
            complete: function() { 
                var welldocument_table = $('#well-document').DataTable({
                    "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false
                        }
                    ]
                });
                $("#tree-doctype-well").jstree("refresh");
                $("#tree-doctype-well").jstree("deselect_all");
                $("#tree-doctype-well").on("changed.jstree", function (e, data) {
                    // console.log(data.selected);
                    var ulala = '/'+data.selected+'/';
                    if (data.selected == 'all') {ulala = '/'};
                    welldocument_table.column(0).search( ulala, true, true, true ).draw();
                });
                $(document).on("submit" , '.transaction-add-doc' , function(event) {
                    var $form = $(this);
                    var formData = {
                        'document': $('input', $form).serialize(),
                    };
                    $.ajax({
                        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url         : base_apps_url+'/transaction/add_cart', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true,
                        success : function(data) {
                        	console.log(formData);
                        	console.log(data);
                            $("#update_cart").html(data.update_cart); 
                            $('button', $form).removeClass().addClass('btn btn-danger disabled').html('<span class="glyphicon glyphicon-shopping-cart"></span> Requested')
                        },
                        error: function(jqXHR, textStatus, errorThrown) { console.log(JSON.stringify(formData)+' | '+textStatus+' | '+errorThrown)},
                        complete : function(){}
                    })
                    event.preventDefault();
                });
            }
        }); 
    }
    this.print_avatar = function(selector){
        var address_well_avatar = base_api+'/avatar/well/'+this.wellId;
        str='';
        $.ajax({
            url: address_well_avatar,
            dataType: 'json',
            beforeSend: function() { $(selector).html('wait.....'); },
            success: function(data) {
                // profileWell = data.wells[0];
                if (data.avatarWell.length < 1) {
                    str += 'picture not available';
                } else {
                    str += '<img src="'+server_address+data.avatarWell[0].path+'" width="100%">';    
                };
            },
            error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
            complete: function() { $(selector).html(str); }
        }); 
    }
    this.print_well_activity = function(selector){
        var address_well_activity = base_api+'/well/'+this.wellId+'/activity';
        var str = '';
        $.ajax({
            url: address_well_activity,
            dataType: 'json',
            beforeSend: function() { $(selector).html('wait.....'); },
            success: function(data) {
                // console.log(data.wellActivities);
                str += '<table class="table" id="table-wellactivity-document"><thead><tr><th class="text-center">Date</th><th class="text-center">Activity</th><th class="text-center">Remark</th></tr></thead><tbody>'
                $.each(data.wellActivities, function(key,val){
                    str += '<tr>';
                    str += '<td class="text-center">'+val.startDate+'</td>';
                    str += '<td class="text-center">'+val.wellActivityTypeId.remark+'</td>';
                    str += '<td>'+val.remark+'</td>';
                    str += '</tr>';
                });
                str += '</tbody></table>';
            },
            error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
            complete: function() { 
                $(selector).html(str); 
                var wellactivity_document_table = $('#table-wellactivity-document').DataTable({
                    "columnDefs": [
                        {
                            "targets": [ 1 ],
                            "visible": true
                        }
                    ]
                });
            }
        }); 
    }
}
//README CLASS FIELD
function Field(fieldId){
    this.fieldId = fieldId;
    var address_field_profile = base_api+'/field/'+this.fieldId;
    $.ajax({
        url: address_field_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            fieldName = data.fields[0].fieldName;
        },
        error: function(jqXHR, textStatus, errorThrown) { fieldName = textStatus},
    }); 
    this.fieldName = fieldName;
    this.print_head_properties = function(selector){
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/field/'+this.fieldId+'">'+fieldName+'</a></strong> | '+this.fieldId);
    }
}
//README CLASS AREA
function Area(areaId){
    this.areaId = areaId;
    var address_area_profile = base_api+'/area/'+this.areaId;
    $.ajax({
        url: address_area_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            areaName = data.areas[0].preferredName;
        },
        error: function(jqXHR, textStatus, errorThrown) { preferredName = textStatus},
    }); 
    this.areaName = areaName;
    this.print_head_properties = function(selector){
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/area/'+this.areaId+'">'+areaName+'</a></strong> | '+this.areaId);
    }
}
//README CLASS WAREHOUSE
function Warehouse(warehouseId){
    this.warehouseId = warehouseId;
    var address_warehouse_profile = base_api+'/warehouse/'+this.warehouseId;
    $.ajax({
        url: address_warehouse_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            warehouseLocation = data.warehouses[0].location;
        },
        error: function(jqXHR, textStatus, errorThrown) { location = textStatus},
    }); 
    this.warehouseLocation = warehouseLocation;
    this.print_head_properties = function(selector){
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/warehouse/'+this.warehouseId+'"> Location '+warehouseLocation+'</a></strong> | '+this.warehouseId);
    }
}
//README CLASS DOCUMENT
function Document(documentId){
	this.documentId = documentId;
	var address_document_profile = base_api+'/document/'+this.documentId;
	$.ajax({
        url: address_document_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            // console.log(data);
            documentName = data.documentProfiles[0].documentName;
            docTypeName = data.documentProfiles[0].docTypeName;
            baName = data.documentProfiles[0].baName;
            businessAssociate = data.documentProfiles[0].businessAssociate;
            dayOfEffectiveDate = data.documentProfiles[0].dayOfEffectiveDate;
            monthOfEffectiveDate = data.documentProfiles[0].monthOfEffectiveDate;
            yearOfEffectiveDate = data.documentProfiles[0].yearOfEffectiveDate;
            isPhysical = data.documentProfiles[0].isPhysical;
            warehouseId = data.documentProfiles[0].warehouseId;
            isDigital = data.documentProfiles[0].isDigital;
            digitalType = data.documentProfiles[0].digitalType;
            docSize = data.documentProfiles[0].docSize;
            path = data.documentProfiles[0].path; 
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log(textStatus+' '+errorThrown) }
    });
    this.print_head_properties = function(selector){
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/document/'+this.documentId+'">'+documentName+'</a></strong> | '+this.documentId);
    } 
    this.print_properties = function(selector){
    	str = '<div class="row"><div class="col-md-6"><ul><li>Document Name : '+documentName+'</li><li>Document Type : ';
    	str += '<div class="col-md-offset-1"><ul>';
    	documentTypeArray = docTypeName.split(";");
    	for(x in documentTypeArray) { 
    		str += '<li>'+documentTypeArray[x]+'</li>'; 
    	}
    	str += '</ul></div></li>';
    	str += '<li>Owner : '+baName+' | '+businessAssociate+'</li><li>Date :  | '+dayOfEffectiveDate+' -  '+monthOfEffectiveDate+' - '+yearOfEffectiveDate+'</li></ul></div><div class="col-md-6"><ul><li>Pysical : '+isPhysical+'</li><li>Warehouse : '+warehouseId+'</li><li>Digital : '+isDigital+'</li><li>Digital Type : '+digitalType+'</li><li>Digital Size : '+docSize+'</li></ul></div></div>';
    	$(selector).html(str);
    }
    this.print_preview_document = function(selector){
    	var address_document_access = base_api+'/user_role/document/'+this.documentId+'/'+userId;
    	$.ajax({
    		url: address_document_access,
	        dataType: 'json',
	        async: false,
	        success: function(data) {
	            // console.log(data);
	            if (data.docUserRoles.length<1) {
	            	$(selector).html('You are not authorized to view this document');
	            } else {
	            	var array_size = data.docUserRoles.length;
	            	var last_authorized = new Date(data.docUserRoles[array_size-1].expiryDate);
	            	var today = new Date();
	            	if (last_authorized - today < 0) {
	            		$(selector).html('You are no longer authorized to view this document, your last authorization was '+last_authorized.getDate()+'-'+(last_authorized.getMonth()+1)+'-'+last_authorized.getFullYear());
	            	} else {
	            		str = 'You are authorized to view this document until '+last_authorized.getDate()+'-'+(last_authorized.getMonth()+1)+'-'+last_authorized.getFullYear();
	            		str += '<hr><div id="print_document"></div>'
	            		$(selector).html(str);
	            		if (isDigital == 1) {
	            			if (digitalType == '.pdf') {
	            				$('#print_document').html('<iframe style="width:100%;height:900px" src="'+base_apps_url+'/assets/plugins/pdfjs/web/viewer.html?file='+server_address+path+'"></iframe>');
	            			} else {
	            				$('#print_document').html("System Can't open the file");
	            			};
	            		};
	            		console.log(server_address+path);
	            	};
	            	
	            };
	        },
	        error: function(jqXHR, textStatus, errorThrown) { console.log(textStatus+' '+errorThrown) }
    	});
    }
}
//README CLASS SEISMIC
function Seismic(seismicId, seismicType){
	this.seismicId = seismicId;
	this.seismicType = seismicType;
	var address_seismic_profile = base_api+'/seis/'+this.seismicId+'/'+this.seismicType;
    $.ajax({
        url: address_seismic_profile,
        dataType: 'json',
        async: false,
        success: function(data) {
            // console.log(data);
		    // console.log(data.seisSets[0]);	
		    // for (key in data.seisSets[0]){
			//     console.log( key + ": " + data.seisSets[0][key]);
			// }
			acqtnDesignId = data.seisSets[0].acqtnDesignId;
            activeInd = data.seisSets[0].activeInd;
            effectiveDate = data.seisSets[0].effectiveDate;
            expiryDate = data.seisSets[0].expiryDate;
            maxLatitude = data.seisSets[0].maxLatitude;
            minLatitude = data.seisSets[0].minLatitude;
            maxLongitude = data.seisSets[0].maxLongitude;
            minLongitude = data.seisSets[0].minLongitude;
            numericId = data.seisSets[0].numericId;	
        },
        error: function(jqXHR, textStatus, errorThrown) { location = textStatus},
    }); 
    
    this.print_head_properties = function(selector){
        $(selector).html('<strong><a target="_blank" href="'+base_apps_url+'/profile/seismic/'+this.seismicId+'/'+this.seismicType+'"> '+this.seismicId+'</a></strong> | '+this.seismicType);
    }
    this.print_properties = function(selector){
    	str = '<div class="row"><ul><li>Seismic : '+this.seismicId+'| '+this.seismicType+'</li><li> Date: '+effectiveDate+'</li></ul></div>'
    	$(selector).html(str);
    }
    this.print_map = function(selector){
    	var seismic_coordinates = [];
    	//kalau line
        seismic_coordinates.push([
	    {lat: minLatitude, lng: minLongitude},
	    {lat: maxLatitude, lng: maxLongitude}]);
	    center_lat = (minLatitude+maxLatitude)/2;
	    center_long = (minLongitude+maxLongitude)/2;
    	str = '<p>'+minLatitude+'</p>'
    	str += '<p>'+minLongitude+'</p>'
    	str += '<p>'+maxLatitude+'</p>'
    	str += '<p>'+maxLongitude+'</p>'
        //kalo acqtn
    	if (this.seismicType == "SEIS_ACQTN_SURVEY") {
    		var seismic_coordinates = [];
    		var _lat = [];
    		var _long = [];
    		str = '<ul>';
    		var address_seismic_child_acqtn = base_api+'/seis/'+this.seismicId+'/acqtn';
    		$.ajax({
		        url: address_seismic_child_acqtn,
		        dataType: 'json',
		        async: false,
		        success: function(data) {
		        	$.each( data.seisAcqtn, function( keyx, valx ) {
		        		_lat.push(valx.minLatitude);_lat.push(valx.maxLatitude);
		        		_long.push(valx.minLongitude);_long.push(valx.maxLongitude)
		        		seismic_coordinates.push([
						    {lat: valx.minLatitude, lng: valx.minLongitude},
						    {lat: valx.maxLatitude, lng: valx.maxLongitude}
						]);
		        		str += '<li><strong><a target="_blank" href="'+base_apps_url+'/profile/seismic/'+valx.seisSetId+'/'+valx.seisSetType+'"> '+valx.seisSetId+'</a></strong> | '+valx.seisSetType+' | '+valx.minLatitude+' '+valx.minLongitude+' '+valx.maxLatitude+' '+valx.maxLongitude+' |</li>'
		            	// str += '<li><strong>''</strong></li>'
		            	// str += valx.seisSetId+' '+valx.seisSetType+' '+valx.minLatitude+' '+valx.minLongitude+' '+valx.maxLatitude+' '+valx.maxLongitude+'<br>';
		            });
		            str += '</ul>'
		            center_lat = (Math.min.apply(Math,_lat)+Math.max.apply(Math,_lat))/2;
		            center_long = (Math.min.apply(Math,_long)+Math.max.apply(Math,_long))/2;
		        },
		        error: function(jqXHR, textStatus, errorThrown) { location = textStatus},
		    }); 
    	};
    	$(selector).html(str);
    	//README 
    	center_map = {lat: center_lat, lng: center_long};
		uluru = {lat: 31.098435, lng: -105.641469};
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: center_map
		});
	  	var contentString = '<strong><a target="_blank" href="'+base_apps_url+'/profile/seismic/'+this.seismicId+'/'+this.seismicType+'"> '+this.seismicId+'</a></strong> | '+this.seismicType;
        // console.log(seismic_coordinates);
		for (var i = 0; i < seismic_coordinates.length; i++) {
			// console.log(seismic_coordinates[i]);
			var seismictPath = new google.maps.Polyline({
		    path: seismic_coordinates[i],
		    strokeColor: '#FF0000',
		    strokeOpacity: 1.0,
		    strokeWeight: 2,
		    map: map
		  });
		}
	  	var infowindow = new google.maps.InfoWindow({
	    	content: contentString
	  	});
	  	var marker = new google.maps.Marker({
	    	position: center_map,
	    	map: map,
	    	title: this.seismicId
	  	});
	  	marker.addListener('click', function() {
	    	infowindow.open(map, marker);
	  	});
    	//README
    }
    this.print_seis_document = function(selector){
        str = '';
        var address_seismic_document = base_api+'/seis/'+this.seismicId+'/'+this.seismicType+'/document';
        $.ajax({
            url: address_seismic_document,
            dataType: 'json',
            async: false,
            success: function(data){
                // console.log(data);
                // $(selector).empty();
                if (data.documentProfiles.length < 1) {
                    str += 'No Data to show'
                } else {
                    str += '<table id="seismic-document" class="table"> <thead><tr><th class="text-center">doctypeId</th><th class="text-center">Date</th><th class="text-center">Document Title</th><th class="text-center">Document Type</th><th class="text-center">Owner</th><th class="text-center">Order</th></tr></thead>';
                    $.each(data.documentProfiles, function(key,val){
                        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                        var doctypename = val.docTypeName.split(";");
                        doctypestrname = '<ul>';
                        for (var x in doctypename){
                            doctypestrname += '<li>'+doctypename[x]+'</li>';
                        }
                        doctypestrname += '</ul>';
                        var doctype = val.docType.split(";");
                        doctypestr = "/";
                        for (var x in doctype){
                            doctypestr += doctype[x]+'/';
                        }
                        str += '<tr>';
                        str += '<td class="text-center">'+doctypestr+'</td>';
                        str += '<td>'+val.dayOfEffectiveDate+' '+months[val.monthOfEffectiveDate-1]+' '+val.yearOfEffectiveDate+'</td>';
                        str += '<td class="text-center"><a target="_blank" href="'+base_apps_url+'/profile/document/'+val.documentId+'">'+val.documentName+'</a></td>';
                        str += '<td class="text-center">'+doctypestrname+'</td>';
                        str += '<td class="text-center">'+val.baName+'</td>';

                        if (array_cart.indexOf(val.documentId) >= 0) {
                            str += '<td class="text-center">'+'<form action="#" method="POST" class=""><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-danger disabled transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Requested</button></form>'+'</td>';
                        } else {
                            str += '<td class="text-center">'+'<form action="#" method="POST" class="transaction-add-doc"><input type="hidden" name="businessAssociate" value="'+val.businessAssociate+'"><input type="hidden" name="baType" value="'+val.baType+'"><input type="hidden" name="documentId" value="'+val.documentId+'"><button type="submit" name="action" class="btn btn-primary transaction-submit"><span class="glyphicon glyphicon-shopping-cart"></span> Request</button></form>'+'</td>';
                        };
                        str += '</tr>';
                    });
                    str += '</table>'
                    $(selector).html(str);

                    
                    $(document).on("submit" , '.transaction-add-doc' , function(event) {
                        var $form = $(this);
                        var formData = {
                            'document': $('input', $form).serialize(),
                        };
                        $.ajax({
                            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url         : base_apps_url+'/transaction/add_cart', // the url where we want to POST
                            data        : formData, // our data object
                            dataType    : 'json', // what type of data do we expect back from the server
                            encode      : true,
                            success : function(data) {
                                console.log(formData);
                                console.log(data);
                                $("#update_cart").html(data.update_cart); 
                                $('button', $form).removeClass().addClass('btn btn-danger disabled').html('<span class="glyphicon glyphicon-shopping-cart"></span> Requested')
                            },
                            error: function(jqXHR, textStatus, errorThrown) { console.log(JSON.stringify(formData)+' | '+textStatus+' | '+errorThrown)},
                            complete : function(){}
                        })
                        event.preventDefault();
                    });
                };
                $(selector).html(str);
                var seisdocument_table = $('#seismic-document').DataTable({
                    "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false
                        }
                    ]
                });
            },
            error: function(jqXHR, textStatus, errorThrown) { console.log('error seis document '+jqXHR+' '+textStatus+' '+errorThrown)}
        });
        
    }
    this.print_seis_data = function(selector){
        str = '';
        str = '<h2>Seismic Data</h2>';
        var address_seismic_data = base_api+'/seis/'+this.seismicId+'/'+this.seismicType+'/data';
        var seis_data_title = {
            seis2DAcqtn : '2D Acquisition',
            seis3DAcqtn : '3D Acquisition',
            seis2DProf : '2D Profile',
            seis3DProf : '3D Profile',
            seis2DInter : '2D Interpretation',
            seis3DInter : '3D Interpretation',
            seis2DProc : '2D Processing',
            seis3DProc : '3D Processing',
            seis2DNavi : '2D Navigation',
            seis3DNavi : '3D Navigation',
            seis2DAdvProc : '2D Advance Processing',
            seis3DAdvProc : '3D Advance Processing'
        }
        $.ajax({
            url: address_seismic_data,
            dataType :'json',
            async : false,
            success: function(data){
                // console.log(data);
                i = 1;
                $.each(data, function(key,val){
                    str += '<h4>'+seis_data_title[key]+'</h4>';
                    if (val.length>0) {
                        str += '<table id="table'+i+'" class="table seis-data">'
                        str += '<thead><tr>'
                        console.log(val[0]);
                        $.each(val[0], function(keyx,valx){
                            str += '<th>'+keyx+'</th>'
                        })
                        str += '</tr></thead><tbody>'
                        $.each(val, function(keyx,valx){
                            str += '<tr>'
                            $.each(valx, function(keyxy,valxy){
                                str += '<td>'+valxy+'</td>'
                            })  
                            str += '</tr>'
                        })
                        str += '</tbody>'
                        str += '</table>'    
                        tablex = '#table'+i
                        console.log(tablex); 
                        // var oTable = $(tablex).dataTable({
                        //     "scrollX": true
                        // });
                    };
                    
                    str += '<hr>';
                    
                    i++
                });
                
            },
            error: function(jqXHR, textStatus, errorThrown) { console.log('error seis document '+jqXHR+' '+textStatus+' '+errorThrown)},
            complete: function(){
                console.log('tes')
                
            }
        });
        // console.log(str);
        $(selector).html(str);
        $('.seis-data').dataTable({
                    "scrollX" : true
                })
    }
}

//README Get Global Variable
function get_array_cart(){
    var array_data = [];
    $.ajax({
        url: base_apps_url+'/transaction/list_cart',
        dataType: 'json',
        async : false,
        success: function(data){
            if (data) {
                array_cart = data;
            } else {
                array_cart = [];
            };
        },
        error: function(jqXHR, textStatus, errorThrown) { array_cart = []; }
    });
}
function get_notification(){
    var str ='';
    var unread = 0;
    var address_get_notification = base_api+'/notification';
    $.ajax({
        url: address_get_notification,
        dataType: 'json',
        xhrFields: {
             withCredentials: true
         },
        success : function(data){
            // console.log(data.notifications);
            $.each(data.notifications, function(key,val) {
               if (val.status == 'Unread') {
                unread++;
                    str += '<li class="text-danger"><i class="turquoise"></i>'+val.content+'</li>';
                } else {
                    str += '<li><i class="turquoise"></i>'+val.content+'</li>';
                };
            });
            $('#notif-content').html(str);
            $('#notif-count').html(unread);
        },
        error: function(jqXHR, textStatus, errorThrown) { 
            console.log('Masih Error')
            console.log(JSON.stringify(jqXHR));
            console.log(jqXHR+' '+textStatus+' '+errorThrown) 
        }
    });
}


//README Print Tree View
function treeview_well(){
    //Function Input = -
    //Function Output = tree view of well | #tree-well
    var address_well_list = base_api+"/well"
    $.getJSON( address_well_list, function( data ) {
        var array_well = [{ "id" : "well", "parent" : "#", "text" : "Well", 'icon' : 'fa fa-database' }];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                var well_object = {
                    "id" : valx.uwi,
                    "parent" : "well",
                    "text" : valx.wellName,
                    "icon" : "fa fa-database"
                }
                array_well.push(well_object);
            });
          });
        $('#tree-well').jstree({ 'core' : {
        'data' : array_well
        } });
    });
    $('#tree-well').on("changed.jstree", function (e, data) {
      content_well(data.selected);
    });
}
function treeview_warehouse(){
    // Function Input = -
    // Function Output = tree view of warehouse | #tree-warehouse
    var address_warehouse_list = base_api+"/warehouse"
    $.getJSON( address_warehouse_list, function( data ) {
        var array_warehouse = [{ "id" : "warehouse", "parent" : "#", "text" : "Warehouse", 'icon' : 'fa fa-bank' }];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                if (!valx.warehouseGroup) {
                    parentWarehouse = "warehouse";
                } else { 
                    parentWarehouse = valx.warehouseGroup.warehouseId;
                };
                var warehouse_object = {
                    "id" : valx.warehouseId,
                    "parent" : parentWarehouse,
                    "text" : valx.warehouseId,
                    "icon" : "fa fa-bank"
                }
                array_warehouse.push(warehouse_object);
            });
        });
        $('#tree-warehouse').jstree({ 'core' : {
            'data' : array_warehouse
        } });
    });
    $('#tree-warehouse').on("changed.jstree", function (e, data) {
      content_warehouse(data.selected);
    });
}
function treeview_area(){
    // Function Input = -
    // Function Output = tree view of warehouse | #tree-warehouse
    var address_treeview_area = base_api+"/treeview/area"
    $.getJSON( address_treeview_area, function( data ) {
        var array_area = [];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                // console.log(valx)
                if (!valx.parent) {
                    parent = "#";
                } else { 
                    parent = valx.parent
                };
                if (valx.type == 'well') {
                    icon = 'fa fa-database'
                } else {
                    if (valx.type == 'area') {
                        icon = "fa fa-home"
                    } else {
                        icon = "fa fa-building"    
                    };
                    
                };
                var area_object = {
                    "id" : valx.child,
                    "parent" : parent,
                    "text" : valx.name,
                    "icon" : icon,
                    "data" : valx.type
                }
                // console.log(area_object);
                array_area.push(area_object);
            });
        });
        $('#tree-area').jstree({ 'core' : {
            'data' : array_area
        } });
    });
    $('#tree-area').on("changed.jstree", function (e, data) {
      // content_warehouse(data.selected);
      // console.log(data.selected);
      // console.log(data.node.data);
      if (data.node.data == 'well') {
        content_well(data.selected);
      };
      if (data.node.data == 'field') {
        content_field(data.selected);
      };
      if (data.node.data == 'area') {
        content_area(data.selected);
      };
    });
}
function treeview_seismic(){
    // Function Input = -
    // Function Output = tree view of seismic | #tree-seismic
    var address_treeview_seismic = base_api+"/treeview/seis"
    $.getJSON( address_treeview_seismic, function( data ) {
        var array_seismic = [{ "id" : "seismic", "parent" : "#", "text" : "Seismic", 'icon' : 'fa fa-folder', 'data' : 'seismic data' }];
        $.each( data, function( key, val ) {
            $.each( val, function( keyx, valx ) {
                // console.log(valx)
                if (!valx.parent) {
                    parent = "seismic";
                } else { 
                    parent = valx.parent+'+'+valx.parentType
                };
                var area_object = {
                    "id" : valx.child+'+'+valx.childType,
                    "parent" : parent,
                    "text" : valx.child,
                    "icon" : 'fa fa-folder',
                    "data" : {'id' : valx.child, 'type' : valx.childType}
                }
                // console.log(area_object);
                array_seismic.push(area_object);
            });
        });
        // console.log(array_seismic);
        $('#tree-seismic').jstree({ 'core' : {
            'data' : array_seismic
        } });
    });
    $('#tree-seismic').on("changed.jstree", function (e, data) {
      content_seismic(data.node.data.id, data.node.data.type);
    });
}
function treeview_doctype_well(){
    var address_tree_document = base_api+'/doctype';
    var array_doctype = [{"id" : "all", "parent":"#", "text": "All", "icon": "fa fa-folder"}];
    $.ajax({
        url: address_tree_document,
        dataType: 'json',
        async: false,
        beforeSend: function() {  $("#tree-doctype-well").html('udah destroy');},
        success: function(data) {
            $.each( data, function( key, val ) {
                $.each( val, function( keyx, valx ) {
                    if (!valx.doctypeGroup) {
                        parentDoctype = "#";
                        icon = false;
                    } else { 
                        parentDoctype = valx.doctypeGroup.doctypeId;
                        icon = "fa fa-folder"
                    };
                    var doctype_object = {
                        "id" : valx.doctypeId,
                        "parent" : parentDoctype,
                        "text" : valx.documentType,
                        "icon" : icon
                    }
                    array_doctype.push(doctype_object);
                });
            });
            $("#tree-doctype-well").jstree({ 
                'core' : {
                    'data' : array_doctype
                } 
            });
        },
        error: function(jqXHR, textStatus, errorThrown) { str = textStatus+' | '+errorThrown},
        complete: function() { 
            // $("#tree-doctype-well").on("changed.jstree", function (e, data) {
            //     console.log(data.selected);
            //     welldocument_table.search( this.value ).draw();
            // });
        }
    }); 
}

get_notification();


setTimeout( function() { alert('Your 30 minutes Session has been expired');window.location.replace(base_apps_url+"/dashboard/logout"); }, timeout_core*1000-2000);
